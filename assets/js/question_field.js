$('#submitButton').click(function()
{
    var question = $('#questionField').val();
    var title = $('#titleField').val();
    
    if(question=="" && title=="")
    {
    	$('#questionEmpty').css("visibility","visible")
        $('#questionForm').addClass("has-error")
		$('#titleEmpty').css("visibility","visible")
        $('#titleForm').addClass("has-error") 
    } 
    else if(title=="")
    {
    	$('#titleEmpty').css("visibility","visible")
        $('#titleForm').addClass("has-error")
    }
    else if(question=="")
    {
        $('#questionEmpty').css("visibility","visible")
        $('#questionForm').addClass("has-error") 
    }
    else
    {
    	var title = $('#titleField').val()
		var isi = $('#questionField').val()
		$(this).button('loading')
		$.ajax(
		{
			url: location.href,
			method: "post",
			data: "title="+title+"&isi="+isi,
			error: function(xhr){
      			alert("An error occured: " + xhr.status + " " + xhr.statusText);
      		},
			success: function()
			{
				$('#submitButton').button('reset')
				$('#titleField').val('')
                $('#questionField').val('')
                $('#timeline').fadeOut("fast")
                $('#timeline').load(location.href+' #timeline')
                $('#timeline').fadeIn("slow")
			}
		});
	}
      
});

$('#questionField').focus(function(){
    $('#questionEmpty').css("visibility","hidden")
    $('#questionForm').removeClass("has-error")
});


$('#titleField').focus(function(){
    $('#titleEmpty').css("visibility","hidden")
    $('#titleForm').removeClass("has-error")
});



	

