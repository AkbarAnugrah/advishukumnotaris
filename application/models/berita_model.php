<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berita_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function update_url($title, $title_url)
	{
		$data = array(
			'Url_title'=>$title_url
			);
		$this->db->where('Judul', $title)
					->update('berita', $data);
	}
	public function select_max($table)
	{
		$this->db->select_max('ID_Berita', 'id');
		return $this->db->get($table);
	}
	public function berita($order_by=null, $limit=null)
	{
		$this->db->from('berita')
				->limit($limit)
				->order_by($order_by,'desc');
		$query = $this->db->get();
		return $query->result();
	}
	public function list_video($order_by=null, $limit=null)
	{
		$this->db->from('berita')
				->limit($limit)
				->order_by($order_by,'desc')
				->where('Video !=','');
		$query = $this->db->get();
		return $query->result();
	}
	public function pagination($perPage, $uri)
	{
		$this->db->from('berita')
					->order_by('ID_Berita','desc');
		$query = $this->db->get('', $perPage, $uri);
		return $query->result();
	}
	public function rows()
	{
		$this->db->from('berita')
					->order_by('ID_Berita','desc');
		$query = $this->db->get();
		return $query->num_rows();
	}
	public function detail_berita($title)
	{
		$this->db->from('berita')
					->where('Url_title', $title);
		$query = $this->db->get();

		if($query->num_rows > 0)
		{
			$this->increase_hit($title);
			return $query->row();
		}
		return FALSE;
	}
	private function increase_hit($title)
	{
		$this->db->query('update berita set hit = hit + 1 where Url_title = "'.$title.'"');
	}
	public function video_utama()
	{
		$this->db->from('berita')
					->where('video_utama','Ya')
					->limit(1);
		$query = $this->db->get();
		return $query->result();
	}
	public function opini()
	{
		$this->db->from('opini')
					->where('Status','Active')
					->limit(1);
		$query = $this->db->get();
		return $query->result();
	}
	public function opini_detail($title)
	{
		$this->db->where('url_title',$title);
		$query = $this->db->get('opini');
		
		if($query->num_rows > 0)
		{
			return $query->row();
		}
		return FALSE;
	}
	public function daftar_opini($limit=null, $offset=null)
	{
		$this->db->order_by('ID_Opini', 'DESC');
		$query = $this->db->get('opini', $limit, $offset);
		return $query->result();
	}
	public function search($param) // search opini
	{
		$this->db->select('Judul, Waktu, Berita as Isi, Foto, Url_title')
					->like('Judul', $param)
					->or_like('Berita', $param);
		$keywords = explode(' ', $param);
		foreach ($keywords as $keyword) {
			$keyword = trim($keyword);
			$this->db->or_like('Judul', $keyword);
			$this->db->or_like('Berita', $keyword);
		}
		$query = $this->db->get('berita');
		return $query->result();
	}
	public function search_opini($param) //search opini
	{
		$this->db->select('Judul, Waktu, Opini as Isi, Foto, Url_title, Nama_narasumber')
					->like('Judul', $param)
					->or_like('Opini', $param)
					->or_like('Nama_narasumber', $param);
		$keywords = explode(' ', $param);
		foreach ($keywords as $keyword) {
			$keyword = trim($keyword);
			$this->db->or_like('Judul', $keyword);
			$this->db->or_like('Opini', $keyword);
			$this->db->or_like('Nama_narasumber', $keyword);
		}
		$query = $this->db->get('opini');
		return $query->result();
	}
	public function update_url_opini($title, $title_url)
	{
		$data = array(
			'Url_title'=>$title_url
			);
		$this->db->where('Judul', $title)
					->update('opini', $data);
	}
}

/* End of file berita_model.php */
/* Location: ./application/models/berita_model.php */