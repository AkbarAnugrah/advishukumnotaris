<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Package_model extends CI_Model
{
	public function get_user_package($id_user)
	{
		$this->db->where('user_id', $id_user)
					->limit(1);
		return $this->db->get('users_packages');
	}
	public function get_package($package_id)
	{
		$this->db->where('id', $package_id)
					->limit(1);
		return $this->db->get('packages');
	}
}