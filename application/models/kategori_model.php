<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori_model extends CI_Model
{
	public function get_all_kategori()
	{
		$this->db->from('kategori');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_kategori($id)
	{
		$this->db->from('kategori')
				->where('id',$id);
		$query = $this->db->get();
		return $query->result();
	}
}

/* End of kategori_model.php */
/* File location: application/models/kategori_model.php */