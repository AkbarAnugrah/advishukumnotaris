<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
	public function get_user_data($id=null)
	{
		if(!$id)
		{
			$id = $this->session->userdata('user_id');
		}
		$this->db->from('users')
					->where('id', $id);
		return $this->get();
	}
	public function get_narasumber_data($id)
	{
		$this->db->from('narasumber')
					->where('ID_Narasumber', $id);
		return $this->db->get();
	}
	public function check_login($email, $password)
	{
		$this->db
				->from('user')
				->where('Email', $email)
				->where('Password', md5($password));
		$query = $this->db->get();

		if($query->num_rows() == 1)
		{
			return $query->result();
		}
		else return FALSE;
	}

	public function narasumber_data($id=null)
	{
		if($id)
		{
			$this->db
					->from('user')
					->where('Hak_akses','3')
					->where('ID_User',$id);
		}
		else
		{
			$this->db
					->from('user')
					->where('Hak_akses','3');
		}
		$query = $this->db->get();
		return $query->result();
	}

	public function foto_narasumber($id)
	{
		return $this->db->select('foto_profil')
					->from('user')
					->where('id',$id)
					->where('hak_akses','3')
					->get()
					->row();
	}	
}