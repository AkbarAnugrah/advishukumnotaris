<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Iklan_model extends CI_model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function get_banner($posisi=null)
	{
		$this->db->from('banner')
					->where('Posisi',$posisi)
					->limit(1);
		$query = $this->db->get();
		return $query->result();
	}

}

/* End of file iklan_model.php */
/* Location: ./application/models/iklan_model.php */