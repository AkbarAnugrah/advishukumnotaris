<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pertanyaan_model extends CI_Model
{
	public function detail($title)
	{
		$this->db->from('pertanyaan')
					->where('Url_title', $title);
		$query = $this->db->get();

		if($query->num_rows > 0)
		{
			$this->increase_hit($title);
			return $query->row();
		}
		return FALSE;
	}
	private function increase_hit($title)
	{
		$this->db->query('update pertanyaan set hit = hit + 1 where Url_title = "'.$title.'"');
	}
	public function update_url($title, $title_url)
	{
		$data = array(
			'Url_title'=>$title_url
			);
		$this->db->where('Judul', $title)
					->update('pertanyaan', $data);
	}
	public function get_user_questions($id_user)
	{
	
		$this->db->from('pertanyaan')
				->where('id_user', $id_user)
				->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();

	}
	public function pertanyaan($order_by, $limit)
	{
		$this->db->from('pertanyaan')
				->where('Jawaban !=','')
				->order_by($order_by,'DESC')
				->limit($limit);
		$query = $this->db->get();
		return $query->result();
	}

	public function delete_question($id)
	{
		$this->db->where('id', $id)
				->delete('pertanyaan');
	}

	public function get_question_and_user($id)
	{
		$this->db->select('pertanyaan.id as id, pertanyaan.id_kategori as id_kategori, pertanyaan.judul as judul, pertanyaan.pertanyaan as pertanyaan,user.nama as nama, user.email as email, user.foto_profil as foto_profil')
				->from('pertanyaan')
				->where('pertanyaan.id', $id)
				->join('user','user.id = pertanyaan.id_user');
		$query = $this->db->get();
		return $query->result();
	}
	public function search($param)
	{
		$this->db->select('Judul, Url_title, Waktu, Pertanyaan as Isi, Jawaban')
					->where('Jawaban !=','')
					->like('Judul', $param)
					->or_like('Pertanyaan', $param);
		$keywords = explode(' ', $param);
		foreach ($keywords as $keyword) {
			$keyword = trim($keyword);
			$this->db->or_like('Judul', $keyword);
			$this->db->or_like('Pertanyaan', $keyword);
		}
		$query = $this->db->get('pertanyaan');
		return $query->result();
	}
	public function rows()
	{
		$this->db->from('pertanyaan')
					->where('Jawaban !=','')
					->order_by('ID_Pertanyaan','desc');
		$query = $this->db->get();
		return $query->num_rows();
	}
	public function pagination($perPage, $uri)
	{
		$this->db->from('pertanyaan')
					->where('Jawaban !=','')
					->order_by('ID_Pertanyaan','desc');
		$query = $this->db->get('', $perPage, $uri);
		return $query->result();
	}
}