<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jawaban_model extends CI_Model
{

	private $table = 'jawaban';

	public function insert_jawaban( $id_pertanyaan, $id_narasumber, $jawaban )
	{
		$data = array(
				'id_pertanyaan' => $id_pertanyaan,
				'id_narasumber' => $id_narasumber,
				'jawaban' => $jawaban
			);
		$this->db->insert('jawaban', $data);
	}

	public function get_jawaban_using_pertanyaan( $id )
	{
		$this->db->from('jawaban')
				->where( 'id_pertanyaan', $id );
		$query = $this->db->get();
		return $query->result();
	}

	public function update_jawaban( $id_pertanyaan, $id_narasumber, $jawaban )
	{
		$data = array(
				'id_narasumber' => $id_narasumber,
				'jawaban' => $jawaban
			);
		$this->db->where('id_pertanyaan', $id_pertanyaan)
					->update('jawaban', $data);
	}

	public function get_answer($question_id)
	{
		$this->db
				->from('jawaban')
				->where('id_pertanyaan', $question_id);
		$query = $this->db->get();

		return $query->result();
	}

	public function get_user_answer($user_id)
	{
		$this->db
				->from($table)
				->where('');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_all_answer()
	{
		$this->db->from($table);
		$query = $this->get();
		
		return $this->db->result();
	}
}