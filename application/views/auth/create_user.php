<!DOCTYPE html>
<html>
      <head>
            <?php $this->load->view('templates/general-css');?>
      </head>
      <body class="full-height" style="background-color:#666666;padding-top:1%">
            <div class="container">
                  <div class="panel panel-default">
                        <div class="panel-heading">
                              <h3 class="panel-title text-bold"><?php echo lang('create_user_heading');?></h3>
                        </div>
                        <div class="panel-body">
                              
                              <span class="lead"><?php echo $message;?></span>

                              <div class="col-lg-6 col-sm-6">
                                    <h3 align="center" style="line-height:40px" id="term_cond">Syarat dan Ketentuan</h3>
                                    <p align="justify">
                                          <span style="margin-left:5%">Advishukumnotaris.com</span> adalah salah satu rubrik Medianotaris.com yang berisi informasi berupa berita, artikel opini, dan tanya-jawab kenotariatan dan masalah hukum yang sering muncul dalam praktek.
                                    </p>
                                    <p align="justify">
                                          <span style="margin-left:5%">Pembaca</span> diberikan kesempatan mengajukan pertanyaan dan akan dijawab oleh narasumber dari notaris senior, pakar hukum, dan dosen hukum. 
                                          Dengan menjadi anggota maka anggota dinyatakan setuju atas ketentuan keanggotaan yang ada.
                                          Ada pun ketentuan berlangganan adalah :
                                    </p>
                                    <br>
                                    <ol>
                                          <li>
                                                membayar biaya langganan selama minimal 4 bulan, atau 6 bulan, 12 bulan atau 24 bulan. Biaya langganan per bulannya adalah Rp 75.000,-
                                          </li>
                                          <br>
                                          <li>
                                                anggota berhak mengajukan pertanyaan yang akan dijawab pakar hukum, pakar  kenotariatan, dan redaksi berdasarkan urutan antrian ;
                                          </li>
                                          <br>
                                          <li>
                                                tanya-jawab ditampilkan di advishukumnotaris.com dengan diedit sedemikian rupa sehingga identitas para pihak dan identitas kasus hukumnya menjadi samar ;
                                          </li>
                                          <br>
                                          <li>
                                                nama dan identitas anggota dirahasiakan ;
                                          </li>
                                          <br>
                                          <li>
                                                materi tanya-jawab adalah masalah kenotariatan, tidak termasuk prosedur SABH atau AHU online dan prosedur lainnya seperti di BKPM, misalnya. Segala isi situs yang dipergunakan anggota untuk kepentingan profesi atau keperluan lainnya adalah merupakan tanggungjawab masing-masing atas segala akibatnya ;
                                          </li>
                                    </ol><br>
                                    
                                    <p align="justify">
                                          <span style="margin-left:5%">Syarat</span> dan ketentuan selengkapnya dapat dilihat <a href="<?php echo base_url('auth/terms_conditions');?>" target="_blank">disini</a>.
                                    </p>
                              </div>
                              <div class="col-lg-6 col-sm-6">
                                    <h3 align="center" style="line-height:40px"><?php echo lang('create_user_subheading');?></h3>


                                    <?php echo form_open("auth/create_user",array('class'=>'form', 'role'=>'form'));?>
                                    <div class="row">
                                          <div class="col-lg-6">
                                                
                                                <div class="form-group">
                                                      <label for="first_name" class="text-bold">Nama: </label><br />
                                                      <?php echo form_input($first_name);?>
                                                </div>

                                                <div class="form-group">
                                                      <?php echo lang('create_user_company_label', 'company');?> <br />
                                                      <?php echo form_input($company);?>
                                                </div>

                                                <div class="form-group">
                                                      <?php echo lang('create_user_pendidikan_label', 'pendidikan');?> <br />
                                                      <?php echo form_input($pendidikan);?>
                                                </div>

                                                <div class="form-group">
                                                      <?php echo lang('create_user_phone_label', 'phone');?> <br />
                                                      <?php echo form_input($phone);?>
                                                </div>
                                                
                                          </div>
                                          <div class="col-lg-6">

                                                <div class="form-group">
                                                      <?php echo lang('create_user_email_label', 'email');?> <br />
                                                      <?php echo form_input($email);?>
                                                </div>

                                                <div class="form-group">
                                                      <label for="password" class="text-bold">Password: </label><br />
                                                      <?php echo form_input($password);?>
                                                </div>

                                                <div class="form-group">
                                                      <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
                                                      <?php echo form_input($password_confirm);?>
                                                </div>

                                                <div class="form-group">
                                                      <?php echo lang('create_user_package', 'package');?> <br />
                                                      <?php echo form_dropdown($package['name'], $package['option'], $package['value'], $package['class'].' '.$package['id']);?>
                                                </div>
                                          </div>
                                    </div>

                                    <label for="pilihan-paket">Pilihan Paket:</label><br>
                                    <ul id="pilihan-paket">
                                          <li>
                                                Paket 1
                                                <ul>
                                                      <li>Masa Aktif 4 bulan</li>
                                                      <li>Harga Rp.300.000.00</li>
                                                </ul>
                                          </li>
                                          <li>
                                                Paket 2
                                                <ul>
                                                      <li>Masa Aktif 6 bulan</li>
                                                      <li>Harga Rp.450.000.00</li>
                                                </ul>
                                          </li>
                                          <li>
                                                Paket 3
                                                <ul>
                                                      <li>Masa Aktif 12 bulan</li>
                                                      <li>Harga Rp.750.000.00</li>
                                                </ul>
                                          </li>
                                    </ul><br><i>* Harga seluruh paket belum  termasuk biaya tambahan maksimal sebesar Rp.1000.00</i><br><br>
                                    <p class="bg-warning padding-2-percent">Kami akan mengirimkan tata cara pembayaran ke alamat email anda segera setelah anda melakukan pendaftaran.</p>
                                    
                                    <div class="checkbox">
                                          <label>
                                                <input type="checkbox" name="terms_conditions" <?php echo ($checked == true) ? 'checked' : '' ;?> > Saya telah membaca dan menyetujui <a href="#term_cond">syarat dan ketentuan</a> yang berlaku
                                          </label>
                                    </div>            
                                    <br>
                                    <div class="text-center">
                                          <a class="btn btn-lg btn-default sharp-edge" href="<?php echo base_url('auth/login')?>"><?php echo lang('create_user_discard_btn');?></a>
                                          <input class="btn btn-lg btn-primary sharp-edge" type="submit" name="submit" value="<?php echo lang('create_user_signin_btn');?>">
                                    </div>
                                    
                                    <?php echo form_close();?>
                              </div>
                              
                        </div>
                  </div>
            </div>
            <?php $this->load->view('templates/general-js');?>
      </body>
</html>
