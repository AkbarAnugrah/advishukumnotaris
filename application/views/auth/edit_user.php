<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <?php $this->load->view('templates/general-css');?>
  </head>
  <body class="full-height" style="background-color:#666666">
        <?php $this->load->view('templates/header');?>
        <div class="container main-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                              <h4 class="panel-title text-bold">User Management</h4>
                        </div>
                        <div class="panel-body">
                            <h1><?php echo lang('edit_user_heading');?></h1>
                            <p><?php echo lang('edit_user_subheading');?></p>

                            <div id="infoMessage"><?php echo $message;?></div>

                            <?php echo form_open(uri_string(), array('class'=>'form', 'role'=>'form'));?>

                                  <div class="form-group">
                                        <span class="text-bold">Nama:</span> <br />
                                        <?php echo form_input($first_name);?>
                                  </div>

                                  <div class="form-group">
                                        <span class="text-bold">Email:</span> <br />
                                        <?php echo $email;?>
                                  </div>

                                  <div class="form-group">
                                        <?php echo lang('create_user_company_label', 'company');?> <br />
                                        <?php echo form_input($company);?>
                                  </div>

                                  <div class="form-group">
                                        <?php echo lang('create_user_pendidikan_label', 'pendidikan');?> <br />
                                        <?php echo form_input($pendidikan);?>
                                  </div>

                                  <div class="form-group">
                                        <?php echo lang('create_user_phone_label', 'phone');?> <br />
                                        <?php echo form_input($phone);?>
                                  </div>

                                  <div class="form-group">
                                        <?php echo lang('edit_user_password_label', 'password');?> <br />
                                        <?php echo form_input($password);?>
                                  </div>

                                  <div class="form-group">
                                        <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><br />
                                        <?php echo form_input($password_confirm);?>
                                  </div>

                                  <?php echo form_hidden('id', $user->id);?>
                                  <?php echo form_hidden($csrf); ?>

                                  <p>
                                  <?php echo form_submit(array('name'=>'submit','class'=>'btn btn-success'), lang('edit_user_submit_btn'));?>
                                  <a href="<?php echo base_url('auth');?>" class="btn btn-default">Batal</a>
                                  </p>

                            <?php echo form_close();?>
                        </div>
                    </div>
              </div>
          </div>
    </div>
        <?php $this->load->view('templates/footer');?>
        <?php $this->load->view('templates/general-js');?>
    </body>
</html>
