<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<?php $this->load->view('templates/general-css');?>
	</head>
	<body class="full-height" style="background-color:#666666">
        <?php $this->load->view('templates/header');?>
        <div class="container main-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                              <h4 class="panel-title text-bold">User Management</h4>
                        </div>
                        <div class="panel-body container-fluid">
							<div id="infoMessage"><?php echo $message;?></div>
							<div class="table-responsive">
								<table class="table table-hover">
									<tr>
										<th class="col-md-2">Nama</th>
										<th class="col-md-2"><?php echo lang('index_email_th');?></th>
										<th class="col-md-2">Paket Mulai</th>
										<th class="col-md-2">Paket Selesai</th>
										<th class="col-md-2"><?php echo lang('index_status_th');?></th>
										<th class="col-md-2"><?php echo lang('index_action_th');?></th>
									</tr>
									<?php foreach ($users as $user):?>
									<tr>
										<td><?php echo $user->first_name;?></td>
										<td><?php echo $user->email;?></td>
										<?php foreach ($user->groups as $group):?>
											<?php if($group->id == "1"){?>
												<?php echo '<td colspan="3">&nbsp;</td>';?>
											<?php }else{?>
							                	<?php foreach($user->package as $package):?>
									                <td><?php echo date("d-M-Y", strtotime($package->start));?></td>
									                <td><?php echo date("d-M-Y", strtotime($package->end));?></td>
													<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link'),array('class'=>'btn btn-md btn-info user_active','style'=>'width:100px')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'),array('class'=>'btn btn-md btn-warning user_inactive','style'=>'width:100px'));?></td>	
								                <?php endforeach;?>
											<?php };?>
						                <?php endforeach;?>
										<td><?php echo anchor("auth/edit_user/".$user->id, '<span class="glyphicon glyphicon-pencil"></span>  Edit', array('class'=>'btn btn-default')) ;?></td>
									</tr>
									<?php endforeach;?>
								</table>
							</div>
							<ul class="pagination pull-right">
								<?php echo $this->pagination->create_links();?>
							</ul>
							<p><?php echo anchor('auth/create_user', lang('index_create_user_link'))?> | <?php echo anchor('auth/create_group', lang('index_create_group_link'))?></p>
						</div>
                  	</div>
            	</div>
      		</div>
		</div>
		<?php $this->load->view('templates/footer');?>
        <?php $this->load->view('templates/general-js');?>
    </body>
</html>