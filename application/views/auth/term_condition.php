<!DOCTYPE html>
<html>
      <head>
            <?php $this->load->view('templates/general-css');?>
      </head>
      <body class="full-height" style="background-color:#666666;padding-top:1%">
            <div class="container">
                  <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                              <div class="panel panel-default">
                                    <div class="panel-heading">
                                          <h3 class="panel-title text-bold"><img style="height:60px" src="<?php echo base_url('assets/images/logo.png')?>">Syarat dan Ketentuan Anggota</h3>
                                    </div>
                                    <div class="panel-body">
                                          <p align="justify">
                                                <span style="margin-left:5%">Advishukumnotaris.com</span> adalah salah satu rubrik medianotaris.com yang berisi informasi berupa berita, artikel opini, dan tanya-jawab kenotariatan dan masalah hukum    yang sering muncul dalam praktek. Advishukumnotaris.com bisa dibaca melalui laptop, komputer desk top, HP, dan komputer tablet di mana pun dan kapan pun Anda mau. Bahkan di luar negeri sekali pun sepanjang peralatan/ gadget Anda terhubung dengan internet. Sehingga Anda tidak kebingungan mencari majalah bila ingin membaca artikel kenotariatan.
                                          </p>
                                          <p align="justify">
                                                <span style="margin-left:5%">Informasi</span> berita dan artikel opini menampilkan kegiatan dunia notaris, PPAT dan kegiatan hukum yang berkaitan dengan notaris dan PPAT. Sedangkan informasi tanya-jawab kenotariatan berisi topik hukum umum dan keperdataan, hukum kenotariatan yang sering ditanyakan ketika bekerja, baik oleh notaris baru, maupun notaris yang sudah senior akibat perkembangan hukum yang cepat. Di dalam rubrik ini pembaca diberikan kesempatan mengajukan pertanyaan dan akan dijawab oleh narasumber dari notaris senior, pakar hukum, dan dosen hukum. 
                                          </p>
                                          <p align="justify">
                                                <span style="margin-left:5%">Dengan</span> menjadi anggota maka anggota dinyatakan setuju atas ketentuan keanggotaan yang ada. Ada pun ketentuan berlangganan adalah :
                                          </p>
                                          <br>
                                          <ol>
                                                <li>
                                                      membayar biaya langganan selama minimal 4 bulan, atau 6 bulan, 12 bulan atau 24 bulan. Biaya langganan per bulannya adalah Rp 75.000,-
                                                </li>
                                                <br>
                                                <li>
                                                      pembayaran melalui ATM atau internet banking ;
                                                </li>
                                                <br>
                                                <li>
                                                      jika sudah membayar, dan mengirim pemberitahuan, dengan pemberitahuan lewat e-mail, yang bersangkutan dinyatakan sebagai anggota  dan akan mendapatkan  user name dan password untuk membuka advisnotaris.com setiap saat ;
                                                </li>
                                                <br>
                                                <li>
                                                      anggota berhak mengajukan pertanyaan yang akan dijawab pakar hukum, pakar  kenotariatan, dan redaksi berdasarkan urutan antrian ;
                                                </li>
                                                <br>
                                                <li>
                                                      tanya-jawab ditampilkan di advishukumnotaris.com dengan diedit sedemikian rupa sehingga identitas para pihak dan identitas kasus hukumnya menjadi samar ;       
                                                </li>
                                                <br>
                                                <li>
                                                      nama dan identitas anggota dirahasiakan ;
                                                </li>
                                                <br>
                                                <li>
                                                      jawaban dalam rubrik tanya-jawab adalah sebagai referensi dan analisa ilmu pengetahuan.  Jika dipergunakan untuk menangani atau menganalisa kasus hukum tertentu untuk kepentingan profesional, maka jawaban dalam rubrik tanya-jawab tersebut tetap harus dikonfirmasikan dengan text book dan peraturan hukum yang terkait. Untuk selanjutnya, keputusan diserahkan pada penanya. Segala isi situs yang dipergunakan anggota untuk kepentingan profesi atau keperluan lainnya adalah merupakan tanggungjawab masing-masing atas segala akibatnya ; 
                                                </li>
                                                <br>
                                                <li>
                                                      namun para narasumber dan redaksi menganalisa permasalahan yang diajukan dan memberikan jawaban berdasarkan kemampuan terbaik para narasumber berdasarkan pengalaman.
                                                </li>
                                          </ol><br>
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
            <?php $this->load->view('templates/general-js');?>
      </body>
</html>
