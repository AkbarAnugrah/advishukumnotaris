<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('templates/general-css');?>
    </head>
    <body class="full-height" style="background-color:#666666">
        <div class="container">
          <div class="row">
              <div class="col-md-4 col-md-offset-4" style="margin-top:5%">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <!--<h3 class="panel-title text-bold"><?php echo lang('login_heading');?></h3>-->
                          <img class="img-responsive" src="<?php echo base_url('assets/images/logo.png');?>"/>
                      </div>
                      <div class="panel-body">
                              <?php if(isset($register_successful)) echo $register_successful;?>
                              <?php echo $message;?>
                              <?php if(isset($subscribe_message)) echo $subscribe_message;?>
                              <?php echo form_open("auth/login",array('class'=>'form', 'role'=>'form'));?>

                                  <div class="form-group">
                                      <?php echo lang('login_identity_label', 'identity');?>
                                      <?php echo form_input($identity);?>
                                  </div>
                                  <div class="form-group">
                                      <?php echo lang('login_password_label', 'password');?>
                                      <?php echo form_input($password);?>
                                  </div>
                                  <div class="checkbox">
                                      <label>
                                          <input type="checkbox" name="remember" value="1" id="remember"> Remember Me
                                      </label>
                                  </div>
                                  <input class="btn btn-info" type="submit" name="submit" value="Masuk">
                                  
                                  <a class="btn btn-success" href="<?php echo base_url('auth/create_user')?>">Pendaftaran</a>

                              <?php echo form_close();?>

                              <a class="pull-right" href="forgot_password" style="line-height:30px"><?php echo lang('login_forgot_password');?></a>
                          
                      </div>
                  </div>              
              </div>
          </div>
        </div>
        <?php $this->load->view('templates/general-js');?>
    </body>
</html>
