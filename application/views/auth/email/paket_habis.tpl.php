<!DOCTYPE html>
<html>
	<head>
		<?php $this->load->view('templates/general-css');?>
	</head>
	<body>
		<div class="container">
			<p>Pelanggan yang terhormat, kami informasikan bahwa paket berlangganan keanggotaan anda di Advishukumnotaris.com telah habis masa berlakunya.</p>
			<p>Silahkan <a href="<? echo base_url();?>" target="_blank">login ke Advishukumnotaris.com</a> untuk melakukan perpanjangan keanggotaan.</p>
			<p>Berikut informasi paket anda yang telah habis masa berlakunya:</p>
			<ul>
				<li>paket: <?php echo $package_name;?></li>
				<li>masa aktif: <?php echo $masa_aktif;?> hari</li>
				<li>mulai: <?php echo date("j-F-Y",strtotime($package_start));?></li>
				<li>selesai: <?php echo date("j-F-Y",strtotime($package_end));?></li>
			</ul>
			<br>
			<p>Pertanyaan, kritik, dan saran silahkan email ke: cs@advishukumnotaris.com / SMS 0812 8687 4455</p>
		</div>
		<?php $this->load->view('templates/general-js');?>
	</body>
</html>