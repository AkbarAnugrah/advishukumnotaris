<html>
	<head>
		<?php $this->load->view('templates/general-css');?>
	</head>
	<body>
		<div class="container">
			<h4>Anda telah melakukan pendaftaran sebagai member di Adivishukum.com!</h4>
			<p>Berikut informasi lengkap akun anda:</p>
			<ul>
				<li>Username</li>
				<li>First Name</li>
				<li>Last Name</li>
				<li>Perusahaan</li>
				<li>Telephone</li>
				<li>Password</li>
				<li>email@email.com</li>
				<li>Paket</li>
				<li>Hari</li>
				<li>Kuota</li>
			</ul>
			<p>Total tagihan yang harus dibayarkan sebesar:</p>
			<p class="text-bold">Rp. 50.050</p>
			<p>Pembayaran dapat dilakukan melalui transfer ke rekening BCA</p>
			<p>0111686768</p>
			<p>a.n. Kustyo Lukie Nugroho</p>
			<div class="row">
				<div class="alert alert-danger col-md-6" role="alert">
					<h5>Perhatian!</h5>
					<p>Nominal uang yang anda kirim <strong>harus</strong> sesuai dengan total tagihan di atas.</p>
				</div>
			</div>
			<p>Silahkan melakukan konfirmasi melalui sms setelah anda melakukan pembayaran</p>
			<div class="row">
				<div class="well col-md-6">
					<p>Ketik :</p>
					<p><span class="text-bold">username</span>&lt;spasi&gt;<span class="text-bold">email</span>&lt;spasi&gt;<span class="text-bold">nominal uang</span></p>
					<p>Kirim ke :</p>
					<p class="text-bold">08174948490</p>
				</div>
			</div>
			<p>Kami akan mengaktifkan akun anda paling lama 24 jam setelah anda melakukan konfirmasi.</p>
		</div>
		<?php $this->load->view('templates/general-js');?>
	</body>
</html>