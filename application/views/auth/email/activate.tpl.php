<!DOCTYPE html>
<html>
	<head>
		<?php $this->load->view('templates/general-css');?>
	</head>
	<body>
		<div class="container">
			<h4>Anda telah melakukan pendaftaran sebagai member di Adivishukum.com!</h4>
			<p>Berikut informasi lengkap akun anda:</p>
			<ul>
				<li>Nama: <?php echo $username;?></li>
				<li>Pekerjaan: <?php echo $company;?></li>
				<li>Telepon: <?php echo $phone;?></li>
				<li>Email: <?php echo $email;?></li>
				<li>Password: <?php echo $password;?></li>
				<li>Paket: <?php echo $package_name;?></li>
				<li>Masa Aktif: <?php echo $days;?> hari</li>
			</ul>
			<p>Total tagihan yang harus dibayarkan sebesar:</p>
			<p class="text-bold">Rp.<?php echo $price;?>,-</p><br>
			<p>Pembayaran dapat dilakukan melalui transfer ke rekening:</p>
			<p>CIMB Niaga, Cabang Gading Serpong, Tangerang, Banten</p>
			<p>No. Rekening: 911 010 037 3163</p>
			<p>a.n. Kustyo Lukie Nugroho</p><br>
			<div class="row">
				<div class="alert alert-danger col-md-6" role="alert">
					<p><strong>Perhatian!</strong></p>
					<p>Nominal uang yang anda kirim <strong>harus</strong> sesuai dengan total tagihan di atas.</p>
				</div>
			</div><br>
			<p>Silahkan melakukan konfirmasi melalui sms setelah anda melakukan pembayaran.</p>
			<div class="row">
				<div class="well col-md-6">
					<p>Ketik :</p>
					<p><span class="text-bold">Nama</span>&lt;spasi&gt;<span class="text-bold">email</span>&lt;spasi&gt;<span class="text-bold">total tagihan</span></p>
					<p>Kirim ke :</p>
					<p class="text-bold">0812 8131 8151</p>
				</div>
			</div>
			<p>Konfirmasi pembayaran juga dapat melalui email, dengan format yang sama seperti konfirmasi pembayaran via sms</p>
			<p>Kirim ke: registration@advishukumnotaris.com</p><br>
			<p>Kami akan mengaktifkan akun anda paling lama 24 jam setelah anda melakukan konfirmasi.</p>
			<p>Pertanyaan, kritik, dan saran silahkan email ke: cs@advishukumnotaris.com / SMS 0812 8687 4455</p>
		</div>
		<?php $this->load->view('templates/general-js');?>
	</body>
</html>