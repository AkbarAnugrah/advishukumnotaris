<!DOCTYPE html>
<html lang="en">
	<head>
		<?php $this->load->view('templates/meta');?>
		<?php $this->load->view('templates/general-css')?>
	</head>
	<body>
		<?php $this->load->view('templates/header');?>
		<div class="container main-container">
			<div class="row">
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12">
							<div id="carousel-example-generic" class="carousel slide img-thumbnail full-width" data-ride="carousel">
							  	<!-- Indicators -->
							  	<!--<ol class="carousel-indicators">
							    	<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
							    	<li data-target="#carousel-example-generic" data-slide-to="1"></li>
							    	<li data-target="#carousel-example-generic" data-slide-to="2"></li>
							  	</ol>-->
							  	<!-- Wrapper for slides -->
							  	<div class="carousel-inner" id="slide">
							  		<?php foreach ($slideshow as $row):?>
							    	<div class="item">
							    		<?php echo anchor('berita/detail/'.$row->Url_title.'', '<img class="img-responsive full-width" src="'.base_url('assets/images/berita/'.$row->Foto.'').'" alt="<?php echo $row->Judul?>">
							      		<div class="carousel-caption text-left" style="width:100%;margin-left:-20% !important;margin-bottom:-3%;background-color:#000;padding:10px">
							        		<h3 class="hidden-xs" style="margin-top:10px">'.$row->Judul.'</h3><p class="hidden-xs">'.substr($row->description, 0,200).'...</p><h4 class="hidden-lg">'.$row->Judul.'</h4>
							      		</div>', array('class' => 'text-white'));?>
							    	</div>
							    	<?php endforeach;?>
							  	</div>
							  	<!-- Controls -->
							  	<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							    	<span class="glyphicon glyphicon-chevron-left"></span>
							  	</a>
							    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							    	<span class="glyphicon glyphicon-chevron-right"></span>
							  	</a>
							</div>
						</div>
					</div><br>
					<div class="row">
						<div class="col-md-6">
							<?php foreach ($banner_kanan_atas as $row):?>
								<img class="img-responsive" src="<?php echo base_url('assets/images/banner/'.$row->Poster.'')?>">
							<?php endforeach;?><br>
							<?php $this->load->view('templates/berita_terbaru');?>
							<div class="panel panel-default">
							  	<div class="panel-heading">
							    	<h3 class="panel-title text-bold">Tetap Bersama Kami</h3>
							  	</div>
							  	<div class="panel-body">
							  		<ul class="list-unstyled no-margin">
							  			<li><a href="https://www.facebook.com/medianotaris.untuknotaris" target="_blank"><img class="img-responsive col-xs-3" src="<?php echo base_url('assets/images/facebook-icon.png')?>" data-toggle="tooltip" data-placement="top" title="medianotaris.untuknotaris"></a></li>
							  			<li><a href="http://www.youtube.com/channel/UC9WT5_KdkO_8ktkSialsfUQ" target="_blank"><img class="img-responsive col-xs-3" src="<?php echo base_url('assets/images/youtube-icon.png')?>" data-toggle="tooltip" data-placement="top" title="Lukie Nugroho"></a></li>
							  			<li><a href="https://twitter.com/medianotaris" target="_blank"><img class="img-responsive col-xs-3" src="<?php echo base_url('assets/images/twitter-icon.png')?>" data-toggle="tooltip" data-placement="top" title="@medianotaris"></a></li>
							  			<li><img class="img-responsive col-xs-3" src="<?php echo base_url('assets/images/instagram-icon.png')?>"></li>
							  		</ul>
							  	</div>
							</div>	
						</div>
						<div class="col-md-6">
							<?php $this->load->view('templates/embedded-timelines');?>
							<div class="panel panel-default">
					  			<?php foreach ($opini as $row):?>
								  	<div class="panel-heading">
								    	<h3 class="panel-title text-bold">Opini <small>Oleh <?php echo $row->Nama_narasumber;?></small></h3>
								  	</div>
								  	<div class="panel-body">
								    	<div class="media">
								    		<a class="pull-left" href="<?php echo site_url('berita/opini/'.$row->Url_title.'');?>">
										    	<img class="img-responsive img-128 media-object" src="<?php echo base_url('assets/images/opini/'.$row->Foto.'');?>" alt="<?php echo $row->Judul;?>">
								    		</a>
									    	<h4 class="media-heading"><a class="text-black" href="<?php echo site_url('berita/opini/'.$row->Url_title.'');?>"><?php echo $row->Judul;?></a></h4>
									    	<span class="text-justify">
									    		<p><?php echo strip_tags(substr($row->Opini, 0,155));?>&hellip;<a href="<?php echo site_url('berita/opini/'.$row->Url_title.'');?>">Read More</a></p><br>
											</span>
										</div>
								  	</div>
					    		<?php endforeach;?>
							</div>
						</div><br>
					</div>			
				</div>
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default" style="text-align:center">
								<img class="img-responsive" src="<?php echo base_url('assets/images/narasumber/header-narasumber.jpg');?>">
								<div class="panel-body">
									<h4 class="col-md-12">Tanya Jawab Hukum Kenotariatan</h4><br><br>
									<a href="<?php echo base_url('tanya_jawab');?>"><button class="btn btn-success" style="border-radius:0px !important"><h4>Ajukan Pertanyaan</h4></button></a>
								</div>
							</div>
						</div>
					</div>
					<?php $this->load->view('templates/pertanyaan_baru_populer');?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
							<?php foreach ($video_utama as $row):?>
							  	<div class="panel-heading">
							    	<h3 class="panel-title"><strong><?php echo $row->Judul;?></strong></h3>
							  	</div>
							  	<div class="panel-body">
									<div class="embed-responsive embed-responsive-4by3">
									  	<iframe class="embed-responsive-item full-width" style="height:225px" src="https://www.youtube.com/embed/<?php echo $row->Video;?>" frameborder="0" allowfullscreen></iframe>
									</div>
							  	</div>
							<?php endforeach;?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php $this->load->view('templates/footer');?>
		<?php $this->load->view('templates/general-js.php')?>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#slide').children().first().addClass('active');
			});
		</script>
	</body>
</html>