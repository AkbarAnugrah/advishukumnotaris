<!DOCTYPE html>
<html>
    <head>
        <title>Advishukum.com - Administrator</title>
        <?php foreach($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
        <?php endforeach; ?>
        <?php $this->load->view('templates/general-css');?>
    </head>
    <body>
        <?php $this->load->view('templates/header');?>
        <div class="container main-container">
            <?php echo $output;?>
        </div>
        <?php $this->load->view('templates/footer');?>
        <?php $this->load->view('templates/general-js');?>
        <?php foreach($js_files as $file): ?>
            <script src="<?php echo $file; ?>"></script>
        <?php endforeach; ?>
    </body>
</html>