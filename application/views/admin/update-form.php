<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('templates/general-css.php')?>
        <!--<link href="<?php echo base_url();?>assets/css/ideplex/jquery.dataTables_themeroller.css" rel="stylesheet">-->
        <link href="<?php echo base_url();?>assets/css/ideplex/DT_bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/css/jquery.dataTables.css" rel="stylesheet">
        <!--<link href="<?php echo base_url();?>assets/css/ideplex/demo_table.css" rel="stylesheet">-->
    </head>
    <body class="skin-black">
        <?php $this->load->view('templates/header-panel')?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="../../img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $nama;?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
<!--Dashboard-->                        
                        <li>
                            <a href="<?php echo base_url('admin')?>">
                                <i class="fa fa-th"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                </section>
                
                <!-- Main content -->
                <section class="content">
                    <!-- row -->
                    <div class="row">
                        <div class="col-md-12">          
                            <div class="box box-danger" id="form-update">
                            	<?php foreach($pertanyaan as $row):?>
    								<form method="post" role="form" >    
										<div class="box-body">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <?php echo validation_errors()?>
        											<input type="hidden" value="<?php echo $row->id?>">
        											<div class="form-group">
        												<label for="form-judul">Judul</label>
        												<input class="form-control" id="form-judul" name="judul" type="text" value="<?php echo $row->judul?>">
        											</div>
        											<div class="form-group">
        												<label for="form-pertanyaan">Pertanyaan</label>
        												<textarea class="form-control" id="form-pertanyaan" name="pertanyaan" rows="10"><?php echo $row->pertanyaan?></textarea>
        											</div>
                                                    <div class="col-md-2">
                                                        <img class="img-thumbnail img-responsive" id="foto-narasumber" src="">
                                                    </div>
                                                    <div class="col-md-3 form-group">
                                                        <label for="narasumber-dropdown">Narasumber</label>
                                                        <select class="form-control" id="narasumber-dropdown" name="narasumber_id">
                                                            <?php foreach($narasumber as $narasumber_row):?>
                                                                <?php foreach($selected_narasumber as $selected):?>
                                                                    <?php if( $narasumber_row->id == $selected->id ){?>
                                                                        <option class="narasumber" selected="selected" value="<?php echo $narasumber_row->id?>"><?php echo $narasumber_row->nama?></option>
                                                                     <?php }else{?>
                                                                        <option class="narasumber" value="<?php echo $narasumber_row->id?>"><?php echo $narasumber_row->nama?></option>           
                                                                    <?php }?>
                                                                <?php endforeach;?>
                                                            <?php endforeach;?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-7 form-group">
                                                        <label for="kategori-dropdown">Kategori</label>
                                                        <select class="form-control" id="kategori-dropdown" name="kategori_id">
                                                            <?php foreach($kategori as $kategori_row):?>
                                                                <?php foreach ($selected_kategori as $selected):?>
                                                                    <?php if( $kategori_row->id == $selected->id) {?>
                                                                        <option class="kategori" selected="selected" value="<?php echo $kategori_row->id?>"><?php echo $kategori_row->kategori?></option> 
                                                                    <?php }else{?>    
                                                                        <option class="kategori" value="<?php echo $kategori_row->id?>"><?php echo $kategori_row->kategori?></option>    
                                                                    <?php }?>
                                                                <?php endforeach;?>
                                                            <?php endforeach;?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="form-jawaban">Jawaban</label>
                                                        <textarea class="form-control" id="form-jawaban" name="jawaban" rows="10"><?php if($jawaban){foreach ($jawaban as $jawaban_row){echo $jawaban_row->jawaban;}}?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="box box-solid box-danger">
                                                        <div class="box-header">
                                                            <h3 class="box-title">Data Pengguna :</h3>
                                                        </div>
                                                        <div class="box-body" style="display: block;">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <img class="img-thumbnail img-responsive" src="<?php echo base_url($row->foto_profil)?>" alt="<?php echo $row->nama?>">
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <!--"../../assets/images/user/2/foto_profil.jpg"-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
    										    </div>
                                            </div>
                                        </div>
										<div class="box-footer">
											<button class="btn btn-primary">Simpan</button>
										</div>
    								</form>
    							<?php endforeach;?>
                            </div>
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <?php $this->load->view('templates/general-js.php')?>
        <script src="<?php echo base_url();?>assets/js/update-form.js"></script>
        <script>
            $(document).ready(function()
            {
                var id_narasumber = $('#narasumber-dropdown option:selected').val();
                detect_narasumber();

                $('#narasumber-dropdown').change(function()
                {
                    id_narasumber = $('#narasumber-dropdown option:selected').val();
                    detect_narasumber();
                });

                function detect_narasumber()
                {
                    $.ajax(
                    {
                        url: "http://localhost/advishukum/admin/get_foto_narasumber",
                        method: "POST",
                        data: "id="+id_narasumber,
                        error: function(xhr){
                            alert("An error occured: " + xhr.status + " " + xhr.statusText);
                        },
                        success: function(response)
                        {
                            $('#foto-narasumber').fadeOut("fast")
                            $('#foto-narasumber').attr("src", response)
                            $('#foto-narasumber').fadeIn("slow")
                        }
                    });
                }
            });
        </script>
    </body>
</html>