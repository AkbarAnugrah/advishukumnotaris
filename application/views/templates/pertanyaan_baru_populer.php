<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title text-bold">Tanya Jawab</h3>
	</div>
	<div class="panel-body">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
		  	<li class="active half-width"><a class="text-bold" href="#terbaru" role="tab" data-toggle="tab">Terbaru</a></li>
		  	<li class="half-width"><a class="text-bold" href="#terpopuler" role="tab" data-toggle="tab">Terpopuler</a></li>
		</ul><br>
		<!-- Tab panes -->
		<div class="tab-content">
		  	<div class="tab-pane fade in active" id="terbaru">
	  			<ul class="list-unstyled">
		  		<?php foreach ($pertanyaan_baru as $row):?>
		  			<li>
			  			<dl>
			  				<dt><?php echo anchor('tanya_jawab/detail/'.$row->Url_title.'', $row->Judul);?></dt>
			  				<dd><?php echo strip_tags(substr($row->Pertanyaan, 0,190));?>&hellip;</dd>
			  			</dl>
		  			</li>
		  		<?php endforeach;?>
	  			</ul>
		  	</div>
		  	<div class="tab-pane fade" id="terpopuler">
		  	<ul class="list-unstyled">
		  		<?php foreach ($pertanyaan_populer as $row):?>
		  			<li>
			  			<dl>
			  				<dt><?php echo anchor('tanya_jawab/detail/'.$row->Url_title.'', $row->Judul);?></dt>
			  				<dd><?php echo strip_tags(substr($row->Pertanyaan, 0,190));?>&hellip;</dd>
			  			</dl>
		  			</li>
		  		<?php endforeach;?>
	  			</ul>
	  		</div>
		</div>
	</div>
</div>