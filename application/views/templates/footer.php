<div class="navbar navbar-default footer no-border" style="margin-bottom:0px">
	<div class="container text-center">
		<img width="200px" src="<?php echo base_url('assets/images/logo.png');?>">
		<img width="150px" src="<?php echo base_url('assets/images/medianotaris.jpg');?>">
		<img id="mediano" width="150px" src="<?php echo base_url('assets/images/mediano.jpg');?>" style="margin:20px">
		<h6>
			<ul class="no-bottom-margin-30 list-inline" style="list-style:none">
				<li><a class="line-height-15" href="<?php echo base_url();?>">Home</a></li>
				<li><a class="line-height-15" href="<?php echo base_url('berita');?>">Berita</a></li>
				<li><a class="line-height-15" href="<?php echo base_url('tanya_jawab');?>">Tanya Jawab</a></li>
				<li><a class="line-height-15" href="<?php echo base_url('about_us');?>">About Us</a></li>
			</ul>
		</h6>
		<div class="page-header"></div>
		<h6 class="text-muted">Hak Cipta &copy; 2013-<?php echo date("Y")?> Advishukumnotaris.com Hak cipta dilindungi oleh undang-undang. Dilarang menerbitkan, menyiarkan, menulis ulang atau menyebarkan informasi dari Advishukumnotaris.com tanpa meminta izin tertulis dari pemilik Advishukumnotaris.com</h6>
	</div>
</div>