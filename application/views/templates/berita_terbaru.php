<div class="panel panel-default">
	<div class="panel-body">
	<?php foreach ($berita_terbaru as $row):?>
		<div class="media">
		  	<a class="pull-left" href="<?php echo base_url('berita/detail/'.$row->Url_title.'.html');?>">
		    	<img class="media-object img-responsive" width="80" height="auto" src="<?php echo base_url('assets/images/berita/'.$row->Foto).'';?>" alt="<?php echo $row->Judul;?>">
		  	</a>
		  	<div class="media-body">
		    	<h5 class="media-heading"><?php echo anchor('berita/detail/'.$row->Url_title.'', $row->Judul);?></h5>
					<?php echo strip_tags(substr($row->Berita, 0,70));?>&hellip;
		  	</div>
		</div>
	<?php endforeach;?>
	<a class="pull-right" href="<?php echo base_url('berita');?>">Lihat Seluruh Berita&hellip;</a>
	</div>
</div>