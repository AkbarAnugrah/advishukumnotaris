<h4>Pertanyaan Terbaru</h4>
<?php foreach ($daftar_pertanyaan as $row):?>
	<div class="media">
	  	<a href="<?php echo site_url('tanya_jawab/detail/'.$row->Url_title.'');?>">
	    	<img class="media-object pull-left img-32" src="<?php echo base_url('assets/images/user_icon.gif');?>" alt="<?php echo $row->Judul;?>">
	    	<h5 class="media-heading"><?php echo $row->Judul;?></h5>
	    </a>
	</div>
<?php endforeach;?>
<ul class="pagination pull-right">
<?php echo $this->pagination->create_links();?>
</ul>