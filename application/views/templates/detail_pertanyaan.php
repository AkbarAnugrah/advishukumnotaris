<!--FB's SDK-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&appId=272112049615999&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--End of FB's SDK-->
<div class="panel panel-default container-fluid">
	<div class="page-header">
	  	<h2 class="no-top-margin-20"><small>Pertanyaan:</small> <?php echo $pertanyaan_detail->Judul;?></h2>
	  	<div class="row">
			<div class="col-md-5">
				<p class="text-muted"><span class="glyphicon glyphicon-calendar"></span> <?php echo date("l, j F Y", strtotime($pertanyaan_detail->Waktu));?></p>
			</div>
			<div class="col-md-3 fb-btn">
				<div class="fb-like" data-href="<?php echo current_url();?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
			</div>
			<div class="col-md-2 fb-btn">	
				<a href="https://twitter.com/share" class="twitter-share-button" data-via="medianotaris" style="margin-left:2%;">Tweet</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
			</div>
		</div>
	</div>
	<div class="row">
	  	<a class="pull-left col-md-2 col-xs-12 text-no-decoration" href="#">
	    	<img class="media-object" width="128" src="<?php echo base_url('assets/images/user_icon.gif');?>" alt="...">
	  		<blockquote style="margin-top:10px">
			  	<footer>Identitas dirahasiakan</footer>
			</blockquote>
	  	</a>
	  	<div class="media-body col-md-10 col-xs-12">
	  		<div class="container-fluid text-justify">
	    		<p><?php echo $pertanyaan_detail->Pertanyaan;?></p>
	  		</div>
	  	</div>
	</div>
</div>
<div class="panel panel-default container-fluid">
	<div class="page-header">
	  	<h2 class="no-top-margin-20"><small>Jawaban:</small></h2>
	</div>
	<div class="row">
	  	<a class="pull-left col-md-2 col-xs-12 text-no-decoration" href="#">
	    	<img class="media-object" width="128" src="<?php echo base_url('assets/images/narasumber/'.$narasumber->Foto.'');?>" alt="...">
	  		<blockquote style="margin-top:10px">
			  	<footer><?php echo $narasumber->Nama;?></footer>
			  	<?php echo $narasumber->Profesi;?>
			</blockquote>
	  	</a>
	  	<div class="media-body col-md-10 col-xs-12">
	  		<div class="container-fluid text-justify">
	    		<?php if(!$this->ion_auth->logged_in()) {?>
					<p><?php echo substr($pertanyaan_detail->Jawaban, 0,150) ;?>...</p><br>
					
					<div class="row bg-warning" style="padding:2%">
						<div class="col-md-12">
							<h4>Silahkan masuk atau mendaftar jika ingin membaca keseluruhan isi jawaban.</h4>
							<?php echo form_open("auth/login",array('class'=>'form-inline', 'role'=>'form'));?>
							    <div class="row">
							    	<div class="col-md-12">
									    <div class="form-group">
									        <?php echo lang('login_identity_label', 'identity');?>
									        <?php echo form_input($identity);?>
									    </div>
								      	<div class="form-group">
								        	<?php echo lang('login_password_label', 'password');?>
								          	<?php echo form_input($password);?>
								      	</div>
								      	<div class="checkbox">
								          	<label>
								            	<input type="checkbox" name="remember" value="1" id="remember"> Remember Me
								          	</label>
								      	</div>
								      	<input class="btn btn-info" type="submit" name="submit" value="Masuk">
								      	<a class="btn btn-success" href="<?php echo base_url('auth/create_user')?>">Pendaftaran</a>  		
							    	</div>
							    </div>
							 <?php echo form_close();?>
						</div>
					</div>
						
				<?php } else { ?>
					<p><?php echo $pertanyaan_detail->Jawaban;?></p>
				<?php } ?>
	  		</div>
	  	</div>
	</div>	
</div>
<?php if($this->ion_auth->logged_in()) {?>
<div class="page-header" style="margin-top:-2%">&nbsp;</div>

<div class="fb-comments" data-href="<?php echo current_url();?>" data-width="700" data-numposts="3" data-colorscheme="light" style="margin-bottom:2%"></div>
<?php }?>
