<div class="row">
	<?php foreach ($list_video as $row):?>
		<div class="col-md-3 col-xs-6">
			<div class="embed-responsive embed-responsive-4by3">
			  	<iframe class="embed-responsive-item full-width" style="height:100px" src="https://www.youtube.com/embed/<?php echo $row->Video;?>?showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
			</div>
			<p><?php echo $row->Judul;?></p>
		</div>
	<?php endforeach;?>
</div>