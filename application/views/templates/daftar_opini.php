<div class="row">
	<?php foreach ($daftar_opini as $row):?>
		<div class="col-md-6 col-xs-6">
			<div class="media">
			  	<a class="pull-left" href="<?php echo base_url('berita/opini/'.$row->Url_title.'');?>">
			    	<img class="media-object img-responsive" width="80" height="auto" src="<?php echo base_url('assets/images/opini/'.$row->Foto).'';?>" alt="<?php echo $row->Judul;?>">
			  	</a>
		    	<h5 class="media-heading"><?php echo anchor('berita/opini/'.$row->Url_title.'', $row->Judul, array('class' => 'text-bold'));?></h5>
				<p class="text-muted"><?php echo $row->Nama_narasumber;?></p>
				<?php echo strip_tags(substr($row->Opini, 0,210));?>&hellip;<a href="<?php echo site_url('berita/opini/'.$row->Url_title.'');?>">Read More</a>
			</div>
		</div>
	<?php endforeach;?>
</div>