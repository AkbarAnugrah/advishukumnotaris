<div class="page-header">
  	<h2>Hasil Pencarian :</h2>
</div>
<div class="row">
<?php foreach ($search_result as $row):?>
	<div class="col-md-12">
		<?php if(isset($row->Nama_narasumber)){?>			
			<h3><a class="text-black" href="<?php echo site_url('berita/opini/'.$row->Url_title.'');?>"><?php echo $row->Judul;?></a></h3>
			<div class="media">
				<a class="pull-left" href="<?php echo site_url('berita/opini/'.$row->Url_title.'');?>">
			    	<img class="img-responsive img-128 media-object" src="<?php echo base_url('assets/images/opini/'.$row->Foto.'');?>" alt="<?php echo $row->Judul;?>">
			  	</a>
		    	<p class="media-heading text-muted"><?php echo date("l, j F Y", strtotime($row->Waktu));?> - Opini</p>
				<p><?php echo strip_tags(substr($row->Isi, 0,200));?>&hellip;</p>
			</div>
		<?php }elseif(isset($row->Jawaban)){?>
			<h3><a class="text-black" href="<?php echo site_url('tanya_jawab/detail/'.$row->Url_title.'');?>"><?php echo $row->Judul;?></a></h3>
			<p class="text-muted"><?php echo date("l, j F Y", strtotime($row->Waktu));?> - Pertanyaan</p>
			<p><?php echo strip_tags(substr($row->Isi, 0,200));?>&hellip;</p>
		<?php }else{?>
			<h3><a class="text-black" href="<?php echo site_url('berita/detail/'.$row->Url_title.'');?>"><?php echo $row->Judul;?></a></h3>
			<div class="media">
				<a class="pull-left" href="<?php echo site_url('berita/detail/'.$row->Url_title.'');?>">
			    	<img class="img-responsive img-128 media-object" src="<?php echo base_url('assets/images/berita/'.$row->Foto.'');?>" alt="<?php echo $row->Judul;?>">
			  	</a>
		    	<p class="media-heading text-muted"><?php echo date("l, j F Y", strtotime($row->Waktu));?> - Berita</p>
				<p><?php echo strip_tags(substr($row->Isi, 0,200));?>&hellip;</p>
			</div>
		<?php };?>
	</div>
<?php endforeach;?>
</div>
<ul class="pagination">
<?php echo $this->pagination->create_links();?>
</ul>