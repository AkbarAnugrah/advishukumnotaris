		
        <script src="<?php echo base_url('assets/js/jquery-2.1.1.min.js')?>" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" type="text/javascript"></script>
        <script>
        $(document).ready(function(){
	        $('.user_active').mouseenter(function(){
	        	$(this).removeClass('btn-info');
	        	$(this).addClass('btn-default');
	        	$(this).html('Nonaktifkan');
	        });
	        $('.user_active').mouseleave(function(){
	        	$(this).removeClass('btn-default');
	        	$(this).addClass('btn-info');
	        	$(this).html('Aktif');
	        });
	        $('.user_inactive').mouseenter(function(){
	        	$(this).removeClass('btn-warning');
	        	$(this).addClass('btn-default');
	        	$(this).html('Aktifkan');
	        });
	        $('.user_inactive').mouseleave(function(){
	        	$(this).removeClass('btn-default');
	        	$(this).addClass('btn-warning');
	        	$(this).html('Nonaktif');
	        });
        });

        var windows_width;

	    windows_width = $(window).width();
		
		resize_fb_comments(windows_width);

		/*$(window).resize(function(){
			
			windows_width = $(window).width();
			resize_fb_comments(windows_width);

		});*/

		function resize_fb_comments(a)
		{
			var width;
	    	if(a > 1300)
	    	{
	    		width = "750";
	    	}
	    	else if(a > 600)
	    	{
	    		width = "600";
	    	}
	    	else if(a > 400)
	    	{
	    		width = "400";
	    	}
	    	else if(a > 300)
	    	{
	    		width = "300";
	    	}
	    	else
	    	{
	    		width = "250";
	    	}
	    	//FB.XFBML.parse(document.getElementById('#fb-comments'));
	    	$('.fb-comments').attr("data-width",width);
		}
        </script>
