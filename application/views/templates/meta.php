<meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $page_title;?></title>
        
        <!--<link rel="canonical" href="<?php echo current_url();?>" />
        <meta property="og:type" content="website" />
        <meta property="og:locale" content="en_US">
        <meta property="og:title" content="<?php echo $page_title;?>">
        <meta property="og:url" content="<?php echo current_url();?>">
        <meta property="og:site_name" content="Advis Hukum Notaris">
        <meta property="fb:app_id" content="272112049615999">
        <meta property="og:description" content="<?php echo $meta['description'];?>">
        
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="title" content="<?php echo $page_title;?>" /> 
        <meta name="keywords" content="<?php echo $meta['keywords'];?>" />
        <meta name="description" content="<?php echo $meta['description'];?>" />
        <?php if(!isset($meta['image'])){?>
        <link rel="image_src" href="<?php echo base_url('assets/images/logo.png');?>" />
        <meta property="og:image" content="<?php echo base_url('assets/images/logo.png');?>">
        <?php }else{?>
        <link rel="image_src" href="<?php echo $meta['image'];?>" />
        <meta property="og:image" content="<?php echo $meta['image'];?>">
        <?php };?>
        <meta name="author" content="Advishukumnotaris.com">-->
        
        <meta property="og:url" content="<?php echo current_url();?>" /> 
        <meta property="og:title" content="<?php echo $page_title;?>" />
        <meta property="og:description" content="<?php echo $meta['description'];?>" /> 
        <meta property="og:image" content="<?php echo (isset($meta['image'])) ? $meta['image'] : base_url('assets/images/narasumber/header-narasumber.jpg');?>" />
