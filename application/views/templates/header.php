<div class="navbar navbar-default footer no-border container" style="margin-bottom:2%;padding-top:0%;padding-bottom:0%">
	<ul class="nav navbar-nav navbar-left"> 
		<li><a class="navbar-brand" href="<?php echo base_url();?>"><img class="img-responsive img-250" src="<?php echo base_url('assets/images/logo-slim.png');?>"></a></li>
		<li><a href="#mediano"><sub>Member of:</sub><img class="img-responsive img-96" src="<?php echo base_url('assets/images/mediano-grey.jpg')?>"></a></li>
	</ul>
	<?php echo form_open("berita/search", array('method'=>'post', 'role'=>'search', 'class'=>'navbar-form navbar-left', 'style'=>'margin-top:1%'));?>
    	<div class="input-group">
				<input class="form-control" name="param" type="text" placeholder="Search" value="<?php echo set_value('param');?>" size="50">
				<span class="input-group-btn">
					<button class="btn btn-default" type="submit" name="submit">
						<span class="glyphicon glyphicon-search"></span>
					</button>
				</span>
		</div><!-- /input-group -->
  	<?php echo form_close();?>
  	<ul class="nav navbar-nav navbar-right hidden-md hidden-xs" style="margin:1%;margin-bottom:0%;margin-top:1.5%">
  		<p class="navbar-text" style="margin-top:0%">Ikuti Kami di: </p>
  		<li><a class="social-icon-link" href="https://www.facebook.com/medianotaris.untuknotaris" target="_blank"><img class="img-responsive img-32" src="<?php echo base_url('assets/images/facebook-icon.png')?>" data-toggle="tooltip" data-placement="top" title="medianotaris.untuknotaris"></a></li>
		<li><a class="social-icon-link" href="http://www.youtube.com/channel/UC9WT5_KdkO_8ktkSialsfUQ" target="_blank"><img class="img-responsive img-32" src="<?php echo base_url('assets/images/youtube-icon.png')?>" data-toggle="tooltip" data-placement="top" title="Lukie Nugroho"></a></li>
		<li><a class="social-icon-link" href="https://twitter.com/medianotaris" target="_blank"><img class="img-responsive img-32" src="<?php echo base_url('assets/images/twitter-icon.png')?>" data-toggle="tooltip" data-placement="top" title="@medianotaris"></a></li>
  	</ul>
</div>
<div class="container" style="background-color:#F8F8F8;border-radius:5px 5px 0 0;border:1px solid #F8F8F8;border-bottom-color:#E7E7E7">
	<div class="navbar navbar-default no-bottom-margin-20" role="navigation" style="border:none">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
			</div><!--.navbar-header-->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    <ul class="nav navbar-nav">
			    	<li><a class="text-bold" href="<?php echo base_url();?>">HOME</a></li>
			        <li><a class="text-bold" href="<?php echo base_url('berita');?>">BERITA</a></li>
			        <li><a class="text-bold" href="<?php echo base_url('tanya_jawab');?>">TANYA JAWAB</a></li>
			    </ul>
			    <div class="nav navbar-nav navbar-right">
			    	<?php if($this->ion_auth->logged_in()) {?>
				      	<p class="navbar-text">Signed in as <?php echo ucwords($this->session->userdata('username'));?></p>
					    <div class="btn-group navbar-btn">
					    	<a class="btn btn-default sharp-edge" href="<?php echo base_url('auth')?>"><span class="glyphicon glyphicon-cog"></span></a>
					    	<?php if($this->ion_auth->is_admin()){?>
							  	<a class="btn btn-default  sharp-edge dropdown-toggle" data-toggle="dropdown">
							    	<span class="text-bold">Administrator </span><span class="caret"></span>
							  	</a>
							  	<ul class="dropdown-menu" role="menu">
								    <li><a href="<?php echo base_url('admin/berita')?>">Berita</a></li>
								    <li class="divider"></li>
								    <li><a href="<?php echo base_url('admin');?>">Pertanyaan Menunggu</a></li>
								    <li><a href="<?php echo base_url('admin/pertanyaan');?>">Pertanyaan Terjawab</a></li>
								    <li class="divider"></li>
								    <li><a href="<?php echo base_url('admin/narasumber')?>">Narasumber</a></li>
								    <li class="divider"></li>
								    <li><a href="http://advishukumnotaris.com:2082/cpsess4734682997/frontend/x3/mail/webmailform.html?user=registration@advishukumnotaris.com&amp;domain=advishukumnotaris.com" target="_blank">registration@advishukumnotaris.com</a></li>
								    <li><a href="<?php echo base_url('auth');?>">User</a></li>
								    <li class="divider"></li>
								    <li><a href="http://advishukumnotaris.com:2082/cpsess4734682997/frontend/x3/mail/webmailform.html?user=cs@advishukumnotaris.com&amp;domain=advishukumnotaris.com" target="_blank">cs@advishukumnotaris.com</a></li>
								    <li><a href="http://advishukumnotaris.com:2082/cpsess4734682997/frontend/x3/mail/webmailform.html?user=noreply@advishukumnotaris.com&amp;domain=advishukumnotaris.com" target="_blank">noreply@advishukumnotaris.com</a></li>
								    <li class="divider"></li>
								    <li><a href="<?php echo base_url('admin/opini');?>">Opini</a></li>
								    <li class="divider"></li>
								    <li><a href="<?php echo base_url('admin/banner');?>">Banner</a></li>
								    <!--<li><a href="<?php echo base_url('admin/profil');?>">Profil</a></li>-->
							  	</ul>
							</div>
					    	<?php };?>
					    <a class="btn btn-default sharp-edge" href="<?php echo base_url('auth/logout');?>"><span class="text-bold">Logout</span></a>	
				    	<?php if(!$this->ion_auth->is_admin()) echo '</div>';?>
			    	<?php } else {?>
			    		<div class="navbar-btn">
			    			<a class="btn btn-success sharp-edge" href="<?php echo base_url('auth/create_user')?>">Pendaftaran Anggota</a>
			    		</div>
			    	<?php }?>
			    </div>
			    
			</div><!-- /.navbar-collapse -->
	</div>
</div>
