<div class="media">
	<h1><?php echo $detail_opini->Judul;?></h1>
	<!--FB's SDK-->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&appId=272112049615999&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<!--End of FB's SDK-->
	<div class="row">
		<div class="col-md-5">
			<p class="text-muted"><span class="glyphicon glyphicon-calendar"></span> <?php echo date("l, j F Y", strtotime($detail_opini->Waktu));?></p>
		</div>
		<div class="col-md-3 fb-btn">
			<div class="fb-like" data-href="<?php echo current_url();?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
		</div>
		<div class="col-md-2 fb-btn">	
			<a href="https://twitter.com/share" class="twitter-share-button" data-via="medianotaris" style="margin-left:2%;">Tweet</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
		</div>
	</div>
	<img class="img-responsive pull-left" src="<?php echo base_url('assets/images/opini/'.$detail_opini->Foto.'')?>">
	<?php if(!$this->ion_auth->logged_in()) {?>

		<p><?php echo strip_tags(substr($detail_opini->Opini, 0,1400)) ;?>...</p><br>
		<div class="row bg-warning" style="padding:2%">
			<div class="col-md-12">
				<h4>Silahkan masuk atau mendaftar jika ingin membaca keseluruhan isi berita.</h4>
				<?php echo form_open("auth/login",array('class'=>'form-inline', 'role'=>'form'));?>
				    <div class="row">
				    	<div class="col-md-12">
						    <div class="form-group">
						        <?php echo lang('login_identity_label', 'identity');?>
						        <?php echo form_input($identity);?>
						    </div>
					      	<div class="form-group">
					        	<?php echo lang('login_password_label', 'password');?>
					          	<?php echo form_input($password);?>
					      	</div>
					      	<div class="checkbox">
					          	<label>
					            	<input type="checkbox" name="remember" value="1" id="remember"> Remember Me
					          	</label>
					      	</div>
					      	<input class="btn btn-info" type="submit" name="submit" value="Masuk">
					      	<a class="btn btn-success" href="<?php echo base_url('auth/create_user')?>">Pendaftaran</a>  		
				    	</div>
				    </div>
				 <?php echo form_close();?>
			</div>
		</div>
			
	<?php } else { ?>
		<p><?php echo $detail_opini->Opini ;?></p>
		<div class="row text-center">	
			<div class="col-md-offset-4 col-md-3">
				<div class="g-ytsubscribe" data-channelid="UC9WT5_KdkO_8ktkSialsfUQ" data-layout="full" data-theme="dark" data-count="default"></div>
			</div>
		</div>
		<div class="page-header" style="margin-top:-2%">&nbsp;</div>

		<div class="fb-comments" data-href="<?php echo current_url();?>" data-width="700" data-numposts="3" data-colorscheme="light" style="margin-bottom:2%"></div>
	<?php }?>
</div>
<br>	