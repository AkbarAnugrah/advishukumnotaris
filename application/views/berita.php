<!DOCTYPE html>
<html lang="en">
	<head>
		<?php $this->load->view('templates/meta');?>
		<?php $this->load->view('templates/general-css');?>
		<style type="text/css"></style>
	</head>
	<body>
		<?php $this->load->view('templates/header');?>
		<div class="container main-container">
			<div class="row">
				<div class="col-md-8">
					<?php if(isset($daftar_berita)){?>
					<div class="page-header" style="margin-top:-10px">
					  	<h1>Daftar Berita<small> dan Opini</small></h1>
					</div>
					<?php $this->load->view('templates/daftar_berita');?>
					<div class="panel panel-default">
					  	<div class="panel-heading">
					    	<h3 class="panel-title text-bold">Opini</h3>
					  	</div>
					  	<div class="panel-body">
					    	<?php $this->load->view('templates/daftar_opini');?>
					  	</div>
					</div>
					<h4 class="">Video<small class="text-muted pull-right">Klik "Watch on Youtube.com" pada kanan bawah video untuk melihat di Youtube</small></h4>
					<div class="page-header no-top-margin-15"></div>
					<?php $this->load->view('templates/list_video');?>
					<div class="row">
						<div class="col-md-6"></div>
						<div class="col-md-6">
						             <a class="twitter-timeline"  href="https://twitter.com/medianotaris" data-widget-id="509350691538608130">Tweet oleh @medianotaris</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          
            			</div>
					</div>
					<?php }elseif(isset($detail_berita)){?>
					<?php $this->load->view('templates/detail_berita');?>
					<?php }elseif(isset($detail_opini)){?>
					<?php $this->load->view('templates/detail_opini');?>
					<?php }elseif(isset($search_result)){?>
					<?php $this->load->view('templates/search_list');}?>
				</div>
				<div class="col-md-4">
					<?php $this->load->view('templates/pertanyaan_baru_populer');?>
					<!--
				  	<?php foreach ($banner_hal_berita as $row):?>
				  		<img class="img-responsive" src="<?php echo base_url('assets/images/banner/'.$row->Poster.'');?>">
				  	<?php endforeach;?><br>-->
				  	<?php $this->load->view('templates/berita_terbaru');?>
				</div>
			</div>
		</div>
		<?php $this->load->view('templates/footer');?>
		<?php $this->load->view('templates/general-js.php')?>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#slide').children().first().addClass('active');
			});
		</script>
	</body>
</html>