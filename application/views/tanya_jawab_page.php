<!DOCTYPE html>
<html>
	<head>
        <?php if(isset($output) && !empty($output)){?>
        <?php foreach($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
        <?php endforeach;} ?>
        <?php $this->load->view('templates/meta');?>
		<?php $this->load->view('templates/general-css');?>
	</head>
	<body>
		<?php $this->load->view('templates/header');?>
		<div class="container main-container">
			<?php if(isset($output) && !empty($output)){?>
				<?php echo $output;?>
			<?php }else if(isset($pertanyaan_detail) && !empty($pertanyaan_detail)){?>
				<div class="row">
					<div class="col-md-8">
						<?php $this->load->view('templates/detail_pertanyaan');?>
					</div>
					<div class="col-md-4">
						<?php $this->load->view('templates/pertanyaan_baru_populer');?>
					  	<?php $this->load->view('templates/berita_terbaru');?>
					</div>
				</div>
			<?php }else{?>
				<?php if(isset($message) && !empty($message)){?>
				<?php echo $message;}?>
				<div class="row">
					<div class="col-md-8">
						<div class="jumbotron orange-border">
						  	<img class="img-responsive" src="<?php echo base_url('assets/images/narasumber/header-narasumber.jpg');?>">
							<h2 class="text-center">Tanya Jawab Hukum Kenotariatan</h2><br><br>
							<div class="row">
								<div class="col-md-2 col-md-offset-1 text-center">
									<img class="img-responsive" src="<?php echo base_url('assets/images/drawing4.png');?>"><br>
									<p>Tulis Pertanyaan</p>
								</div>
								<div class="col-md-2 col-md-offset-2 text-center">
									<img class="img-responsive" src="<?php echo base_url('assets/images/paperplane.png');?>"><br>
									<p>Tunggu...</p>
								</div>
								<div class="col-md-2 col-md-offset-2 text-center">
									<img class="img-responsive" src="<?php echo base_url('assets/images/close13.png');?>"><br><br>
									<p>Cek Email</p>
								</div>
							</div><br><br>
							<div class="row">
								<a class="btn btn-success btn-lg col-md-5 col-md-offset-4 col-xs-12" href="<?php echo base_url('tanya_jawab/table/add');?>" role="button">Klik Untuk Mulai Bertanya</a>
							</div><br>
							<div class="row">
						  		<a class="btn btn-default btn col-md-5 col-md-offset-4 col-xs-12" href="<?php echo base_url('tanya_jawab/table');?>" role="button">Lihat Pertanyaan Anda Sebelumnya</a>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<?php $this->load->view('templates/list_pertanyaan');?>
					  	<a class="twitter-timeline"  href="https://twitter.com/medianotaris" data-widget-id="509350691538608130">Tweet oleh @medianotaris</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          
					</div>
				</div>
			<?php }?>
		</div>
		<?php $this->load->view('templates/footer');?>
		<?php $this->load->view('templates/general-js');?>
		<?php if(isset($output) && !empty($output)){?>
        <?php foreach($js_files as $file): ?>
            <script src="<?php echo $file; ?>"></script>
        <?php endforeach; }?>
	</body>
</html>