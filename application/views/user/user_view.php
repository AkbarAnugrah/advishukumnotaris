<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('templates/general-css.php')?>
        <style type="text/css">
            #titleEmpty{visibility:hidden;}
            #questionEmpty{visibility:hidden;}
        </style>
    </head>
    <body class="skin-black">
        <?php $this->load->view('templates/header-panel')?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="../../img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $nama;?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
<!--Dashboard-->                        
                        <li>
                            <a href="<?php echo base_url('user')?>">
                                <i class="fa fa-th"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <ol class="breadcrumb">
                        <li><i class="fa fa-th"></i>&nbsp;&nbsp;Home</li>
                    </ol>
                </section>
                            
                <!-- Main content -->
                <section class="content">
                    <!-- row -->
                    <div class="row">                        
                        <div class="col-md-8">
<!-- Timeline -->           <div class="box box-primary">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box-header">
                                            <h3 class="box-title">Question</h3>
                                        </div>
                                    </div>
                                </div>    
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box-body">
                                            <form class="form-horizontal col-md-12" id="form_1" action="<?php echo base_url('user')?>" method="post"  role="form">
                                                <div class="form-group" id="titleForm">
                                                    <label class="control-label" id="titleEmpty" for="titleField">
                                                        <i class="fa fa-times-circle-o"></i>
                                                        The Field is Empty!
                                                    </label>
                                                    <input class="form-control" id="titleField" name="title" type="text" placeholder="title...">
                                                </div>
                                                <div class="form-group" id="questionForm">
                                                    <label class="control-label" id="questionEmpty" for="questionField">
                                                            <i class="fa fa-times-circle-o"></i>
                                                            The Field is Empty!
                                                    </label>
                                                    <textarea class="form-control" id="questionField" name="question" placeholder="your question here..." rows="3"></textarea>
                                                    <div id="coba"></div>
                                                </div>
                                            </form>
                                        </div>    
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box-footer">
                                            <button class="btn-sm btn-default" id="submitButton">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="timeline" id="timeline">
<!--Showing Questionss-->        
                                <?php foreach($pertanyaan as $row){?>
                                    <?php $PostTime = strtotime($row->waktu)?>
                                    <?php $QuestionTime = date('Y-m-d', $PostTime)?>
                                    <?php if($QuestionTime < $thisTime){?>
                                        <li class="time-label">
                                            <span class="bg-green">
                                                <?php echo date('j M. Y', $PostTime)?>
                                                <?php $thisTime = $QuestionTime?>
                                            </span>
                                        </li>
                                    <?php }?>
                                    <li>
                                        <i class="fa fa-envelope bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o">&nbsp;</i><?php echo date('H:i',$PostTime)?></span>
                                            <h3 class="timeline-header"><a href="#"><?php echo $nama?></a>&nbsp;posted a question</h3>
                                            <div class="timeline-body">
                                                <h4><?php echo $row->judul?></h4>
                                                <?php echo substr($row->isi, 0,300)?>&hellip;
                                            </div>
                                            <div class='timeline-footer'>
                                                <div class='timeline-footer'>
                                                    <a class="btn btn-primary btn-xs">Read more</a>
                                                    <a class="btn btn-danger btn-xs">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
<!--Showing Answer-->               <?php $jawaban = $user->fetching_answer($row->id)?>
                                    <?php if($jawaban){?>
                                        <?php foreach ($jawaban as $row2){?>
                                            <li>
                                                <i class="fa fa-comments bg-yellow"></i>
                                                <div class="timeline-item">
                                                <span class="time"><i class="fa fa-clock-o">&nbsp;</i><?php echo date('j M Y', strtotime($row2->waktu))?></span>
                                                <h3 class="timeline-header"><a href="#"><?php echo $nama?></a>&nbsp;answered your question</h3>
                                                <div class="timeline-body">
                                                    <?php echo substr($row2->isi, 0,300)?>&hellip;
                                                </div>
                                                <div class='timeline-footer'>
                                                    <div class='timeline-footer'>
                                                        <a class="btn btn-primary btn-xs">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                <?php }?>
                        	</ul>
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <?php $this->load->view('templates/general-js.php')?>
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/question_field.js"></script>
    </body>
</html>