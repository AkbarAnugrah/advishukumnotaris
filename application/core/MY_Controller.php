<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	protected $data;
	protected $referer;
	public function __construct()
	{
		parent::__construct();
		/*$this->load->library('user_agent');
		$robot = $this->agent->robot();*/
		
		$flag = false;
		if (in_array($_SERVER['HTTP_USER_AGENT'], array(
		  'facebookexternalhit/1.1 (+https://www.facebook.com/externalhit_uatext.php)',
		  'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)'
		))) {
		  $flag = true;
		};

		$flag2 = false;
		if(strstr(strtolower($_SERVER['HTTP_USER_AGENT']), "googlebot"))
		{
			$flag2 = true;    
		}

		$this->session->set_userdata('last_page', current_url());

		preg_match('@^(?:http://)?([^/]+).([^/]+).([^/]+).([^/]+).([^/]+)@i', $this->session->userdata('last_page'), $matches);

		$title = rtrim(rtrim($matches[5], 'html'),'.');

		//Pada advis online menggunakan array $matches dengan indeks - 1 dari indeks $matches localhost

		if( !$this->ion_auth->logged_in() && !$flag && !$flag2 && !( ($matches[4]=="detail" || $matches[4]=="opini" ) && $matches[5]) )
		{
			
			redirect(base_url('auth/login'),'refresh');
		}
	}
}