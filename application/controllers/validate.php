<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Validate extends CI_Controller
{
	public function __constructor()
	{
		parent::__construct();
		
	}

	public function index()
	{

		if($this->input->post())
		{
			$this->load->library('form_validation');

			 $rules = array(
				        array('field' => 'email',
				              'label' => 'Email Address',
				              'rules' => 'trim|required|max_length[50]|xss_clean'),
				        array('field' => 'password',
				              'label' => 'Password',
				              'rules' => 'trim|required|max_length[50]|xss_clean') 
				    ) ;
			$this->form_validation->set_rules($rules);

			if($this->form_validation->run()==TRUE)
			{
				$this->load->model('user_model');
				$email = $this->input->post('email');
				$password = $this->input->post('password');
				$var_cek = $this->user_model->check_login($email,$password);
				
				if($var_cek)
				{
					foreach($var_cek as $row)
					{
						
						$data_user = array(
							'ID_User' => $row->ID_User, 
							'Hak_akses' => $row->Hak_akses, 
							'Nama' => $row->Nama,
							'Email'=>$row->Email,
							'Status'=>$row->Status
							);

						$this->session->set_userdata($data_user);
					
						if($row->Hak_akses=="Administrator")
						{
							redirect(base_url('admin'),'refresh');
						}
						else if($row->Hak_akses=="Member")
						{
							if($row->Status=="Active")
							{
								redirect(base_url('user'),'refresh');
							}
						}
					}
				}
			}	
		}
		redirect(base_url(),'refresh');
	}

	public function logout()
	{
		$data_user = array(
			'id' => null,
			'hak_akses' => null,
			'nama' => null,
			'email' => null 
			);
		$this->session->unset_userdata($data_user);
		redirect('main','refresh');
	}

}
