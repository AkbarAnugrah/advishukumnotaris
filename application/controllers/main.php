<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Main extends MY_Controller
{
	public function __constructor()
	{
		parent::__constructor();
	}

	public function index()
	{
		$this->load->model(array('berita_model','iklan_model','pertanyaan_model'));
		$data['slideshow'] = $this->berita_model->berita('ID_Berita',3);
		$data['pertanyaan_populer'] = $this->pertanyaan_model->pertanyaan('hit',4);
		$data['pertanyaan_baru'] = $this->pertanyaan_model->pertanyaan('ID_Pertanyaan',4);
		$data['berita_terbaru'] = $this->berita_model->berita('ID_Berita',6);
		$data['banner_kanan_atas'] = $this->iklan_model->get_banner('Kanan Atas');
		//$data['banner_kanan_tengah'] = $this->iklan_model->get_banner('Kanan Tengah');
		//$data['banner_kiri_bawah'] = $this->iklan_model->get_banner('Kiri Bawah');
		$data['video_utama'] = $this->berita_model->video_utama();
		$data['opini'] = $this->berita_model->opini();
		$this->load->helper('form');	
		$data['title'] = 'AdvisHukum.com';
		//meta-tag
		$data['page_title'] = 'Advishukumnotaris.com';
		$data['meta']['description'] = 'Tanya Jawab Hukum Kenotariatan';
		$data['meta']['keywords'] = 'Tanya,Jawab,Hukum,Notaris,Kenotariatan,Advishukumnotaris.com';
		/*------------------------------------------------------------------------------------------*/
		$this->load->view('main_page',$data);
	}
	
}
/* End of file main.php */
/* Location: ./application/controllers/main.php */