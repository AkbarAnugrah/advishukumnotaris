<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tanya_jawab extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('table','grocery_CRUD'));
	}

	public function index()
	{
		//Main Content - list berita
		$this->load->library('pagination');
		$this->load->model('pertanyaan_model');
		$config['base_url'] = base_url('tanya_jawab/index/');
		$config['total_rows'] = $this->pertanyaan_model->rows();
		$config['num_links'] = 4;
		$config['per_page'] = 5;
		$config['uri_segment'] = 3;
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		//meta-tag
		$data['page_title'] = 'Tanya Jawab - Advishukumnotaris.com';
		$data['meta']['description'] = 'Tanya Jawab Hukum Kenotariatan';
		$data['meta']['keywords'] = 'Tanya,Jawab,Hukum,Notaris,Kenotariatan,Advishukumnotaris.com';
		/*------------------------------------------------------------------------------------------*/
		$data['daftar_pertanyaan'] = $this->pertanyaan_model->pagination($config['per_page'], $this->uri->segment(3));
		$this->load->view('tanya_jawab_page', $data);
	}
	public function table()
	{
		$this->config->set_item('url_suffix', '');
		/* Fitur Kuota Pertanyaan 
		if(!$this->ion_auth->check_attemps())
		{
			$data['message'] = '<div class="alert alert-danger" role="alert"><h3>Oops, kuota anda telah habis.</h3></div>';
			redirect(base_url('tanya_jawab'),'refresh');
		}*/

		$crud = new grocery_CRUD();
		$crud->set_table('pertanyaan')
				->set_subject('Pertanyaan')
				//->callback_after_insert(array($this,'increase_attemps'))
				->callback_after_insert(array($this,'url_title_pertanyaan'))
				->set_relation('ID_Narasumber','narasumber','Nama')
				->display_as('ID_Narasumber','Narasumber')
				->set_relation_n_n('Kategori','kategori_pertanyaan','kategori','ID_Pertanyaan','ID_Kategori','Kategori')
				->columns('ID_Pertanyaan','Waktu','Judul','Pertanyaan','Jawaban','Kategori','ID_Narasumber')
				->add_fields('ID_User','Judul','Pertanyaan')
				->field_type('ID_User', 'hidden', $this->session->userdata('user_id'))
				->unset_delete()
				->unset_edit()
				->where('ID_User', $this->session->userdata('user_id'))
				->order_by('ID_Pertanyaan','ASC');
		$config = array(
			array(
				'field' => 'Judul',
				'label' => 'Judul',
				'rules' => 'trim|required|xss_clean|max_length[100]'
				),
			array(
				'field' => 'Pertanyaan',
				'label' => 'Pertanyaan',
				'rules' => 'required|xss_clean'
				)
			);
		$crud->set_rules($config);
		$output = $crud->render();
		$this->load->view('tanya_jawab_page', $output);
	}
	public function detail($title=null)
	{
		if(!isset($title) && empty($title))
		{
			redirect('tanya_jawab','refresh');
		}
		//Main content
		$this->load->model(array('pertanyaan_model','berita_model','iklan_model','user_model'));
		$data['pertanyaan_detail'] = $this->pertanyaan_model->detail($title);
		$data['narasumber'] = $this->user_model->get_narasumber_data($data['pertanyaan_detail']->ID_Narasumber)->row();
		/*----------------------------------------------------------------------------------------------------*/
		//meta-tag
		$data['page_title'] = $data['pertanyaan_detail']->Judul.' - Advishukumnotaris.com';
		$data['meta']['description'] = $data['pertanyaan_detail']->description;
		$data['meta']['keywords'] = $data['pertanyaan_detail']->keywords;
		/*------------------------------------------------------------------------------------------*/
		$data['pertanyaan_populer'] = $this->pertanyaan_model->pertanyaan('hit',3);
		$data['pertanyaan_baru'] = $this->pertanyaan_model->pertanyaan('ID_Pertanyaan',3);
		$data['berita_terbaru'] = $this->berita_model->berita('ID_Berita',5);
		$data['banner_hal_berita'] = $this->iklan_model->get_banner('Hal Berita');

		/*-----------------------------------------------------------------*/
		//if user does not login yet
		$data['identity'] = array('name' => 'identity',
				'class' => 'form-control',
				'id' => 'identity',
				'type' => 'text',
				'placeholder' => 'Email...',
			);
		$data['password'] = array('name' => 'password',
			'class' => 'form-control',
			'id' => 'password',
			'type' => 'password',
			'placeholder' => 'Password...',
		);
		
		$this->load->view('tanya_jawab_page', $data);
	}
	public function increase_attemps($post_array, $primary_key)
	{
		if(!isset($post_array) && !empty($post_array))
		{
			redirect('tanya_jawab/table','refresh');
		}
		$this->db->query('Update users_packages set attemp = attemp + 1 where user_id = '.$post_array['ID_User'].'');
	}
	public function url_title_pertanyaan($post_array, $primary_key)
	{
		if(!isset($post_array) && empty($post_array))
		{
			show_404();
		}
		$url_title = url_title($post_array['Judul'], '_', TRUE);
		$this->load->model('pertanyaan_model');
		$this->pertanyaan_model->update_url($post_array['Judul'], $url_title);
	}
	//public function detail($)
}

/* End of tanya_jawaba.php */
/* File location: application/controller/tanya_jawab.php */