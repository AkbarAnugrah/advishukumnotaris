<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Berita extends MY_Controller
{
	public function __constructor()
	{
		parent::__constructor();
		$this->load->model(array('berita_model','iklan_model','pertanyaan_model'));
	}
	public function index()
	{
		redirect(base_url('berita/list_berita'));
	}
	public function list_berita()
	{
		$this->load->model(array('berita_model','iklan_model','pertanyaan_model'));
		//Main Content - list berita
		$this->load->library('pagination');
		$config['base_url'] = base_url('berita/list_berita/');
		$config['total_rows'] = $this->berita_model->rows();
		$config['num_links'] = 4;
		$config['per_page'] = 8;
		$config['uri_segment'] = 3;
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$data['daftar_berita'] = $this->berita_model->pagination($config['per_page'], $this->uri->segment(3));
		/*---------------------------------------------------------------------------------------------------*/
		//List opini
		$data['daftar_opini'] = $this->berita_model->daftar_opini(2,'');
		/*-------------------------------------------------------------*/
		//List Video
		$data['list_video'] = $this->berita_model->list_video('hit',4);
		/*-------------------------------------------------------------*/
		//meta-tag
		$data['page_title'] = 'Berita - Advishukumnotaris.com';
		$data['meta']['description'] = 'Daftar Berita Advishukumnotaris.com';
		$data['meta']['keywords'] = 'Advishukumnotaris.com,Berita,Daftar Berita,List,Kumpulan Berita';
		/*------------------------------------------------------------------------------------------*/
		$data['pertanyaan_populer'] = $this->pertanyaan_model->pertanyaan('hit',3);
		$data['pertanyaan_baru'] = $this->pertanyaan_model->pertanyaan('ID_Pertanyaan',3);
		$data['berita_terbaru'] = $this->berita_model->berita('ID_Berita',6);
		$data['banner_hal_berita'] = $this->iklan_model->get_banner('Hal Berita');
		
		$this->load->view('berita',$data);
	}
	public function detail($title=null)
	{
		if(!$title)
		{
			redirect(base_url('berita'),'refresh');
		}
		$this->load->model(array('berita_model','iklan_model','pertanyaan_model'));
		//Main Content
		$data['detail_berita'] = $this->berita_model->detail_berita($title);
		if($data['detail_berita']==FALSE)
		{
			show_404();
		}
		/*-----------------------------------------------------------------*/
		//meta-tag
		$data['page_title'] = $data['detail_berita']->Judul;
		$data['meta']['keywords'] = $data['detail_berita']->keywords;
		$data['meta']['description'] = $data['detail_berita']->description;
		$data['meta']['image'] = base_url('assets/images/berita/'.$data['detail_berita']->Foto);
		/*-----------------------------------------------------------------*/
		$data['pertanyaan_populer'] = $this->pertanyaan_model->pertanyaan('hit',3);
		$data['pertanyaan_baru'] = $this->pertanyaan_model->pertanyaan('ID_Pertanyaan',3);
		$data['berita_terbaru'] = $this->berita_model->berita('ID_Berita',5);
		$data['banner_hal_berita'] = $this->iklan_model->get_banner('Hal Berita');
		
		/*-----------------------------------------------------------------*/
		//if user does not login yet
		$data['identity'] = array('name' => 'identity',
				'class' => 'form-control',
				'id' => 'identity',
				'type' => 'text',
				'placeholder' => 'Email...',
			);
		$data['password'] = array('name' => 'password',
			'class' => 'form-control',
			'id' => 'password',
			'type' => 'password',
			'placeholder' => 'Password...',
		);

		$this->load->view('berita',$data);
	}
	public function opini($title=null)
	{
		if(!$title)
		{
			redirect(base_url('berita'),'refresh');
		}
		$this->load->model(array('berita_model','iklan_model','pertanyaan_model'));
		//Main Content
		$data['detail_opini'] = $this->berita_model->opini_detail($title);
		/*------------------------------------------------------------------*/
		//meta-tag
		$data['page_title'] = $data['detail_opini']->Judul.' - Advishukumnotaris.com';
		$data['meta']['keywords'] = $data['detail_opini']->keywords;
		$data['meta']['description'] = $data['detail_opini']->description;
		$data['meta']['image'] = base_url('assets/images/opini/'.$data['detail_opini']->Foto);
		/*-----------------------------------------------------------------*/
		$data['pertanyaan_populer'] = $this->pertanyaan_model->pertanyaan('hit',3);
		$data['pertanyaan_baru'] = $this->pertanyaan_model->pertanyaan('ID_Pertanyaan',3);
		$data['berita_terbaru'] = $this->berita_model->berita('ID_Berita',5);
		$data['banner_hal_berita'] = $this->iklan_model->get_banner('Hal Berita');

		/*-----------------------------------------------------------------*/
		//if user does not login yet
		$data['identity'] = array('name' => 'identity',
				'class' => 'form-control',
				'id' => 'identity',
				'type' => 'text',
				'placeholder' => 'Email...',
			);
		$data['password'] = array('name' => 'password',
			'class' => 'form-control',
			'id' => 'password',
			'type' => 'password',
			'placeholder' => 'Password...',
		);

		$this->load->view('berita',$data);
	}
	public function search()
	{
		if(!$this->input->post('param') && !$this->input->post('submit'))
		{
			//redirect(base_url(),'refresh');
		}
		$this->load->model(array('berita_model','iklan_model','pertanyaan_model'));
		$this->load->library(array('form_validation','user_agent'));
		$this->form_validation->set_rules('param','Parameter','required|trim|max_length[50]|xss_clean');
		if($this->form_validation->run() == FALSE)
		{
			if ($this->agent->is_referral() && $this->agent->referrer())
			{
			    redirect($this->agent->referrer(), 'refresh');
			}
			else
			{
				redirect(base_url(),'refresh');
			}
		}
		else
		{
			//Main Content
			$this->load->library('pagination');

			$berita_result = $this->berita_model->search($this->input->post('param'));
			$pertanyaan_result = $this->pertanyaan_model->search($this->input->post('param'));
			$opini_result = $this->berita_model->search_opini($this->input->post('param'));
			$data['search_result'] = array_merge($berita_result, $pertanyaan_result, $opini_result);
			function cb($a, $b) {
			    return strtotime($b->Waktu) - strtotime($a->Waktu);
			}
			usort($data['search_result'], 'cb');
			/*-------------------------------------------------------------------------------*/
			//meta-tag
			$data['page_title'] = $this->input->post('param');
			$data['meta']['keywords'] = $this->input->post('param');
			$data['meta']['description'] = $this->input->post('param');
			/*-----------------------------------------------------------------*/
			$data['pertanyaan_populer'] = $this->pertanyaan_model->pertanyaan('hit',3);
			$data['pertanyaan_baru'] = $this->pertanyaan_model->pertanyaan('ID_Pertanyaan',3);
			$data['berita_terbaru'] = $this->berita_model->berita('ID_Berita',5);
			$data['banner_hal_berita'] = $this->iklan_model->get_banner('Hal Berita');
			
			$this->load->view('berita',$data);
		}
	}
}
/* End of file berita.php */
/* Location: ./application/controllers/berita.php */