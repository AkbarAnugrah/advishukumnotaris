<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller
{
	public function __construct()
	{

		parent::__construct();
		if(!$this->ion_auth->is_admin())
		{
			show_404();
		}
		$this->load->database();
		$this->load->library(array('table','grocery_CRUD'));
		$this->config->set_item('url_suffix', '');

	}
	//Pertanyaan belum terjawab
	public function index()
	{ 	
		$crud = new grocery_CRUD();
		$crud->set_table('pertanyaan')
				->set_subject('Pertanyaan')
				->callback_after_insert(array($this, 'url_title_pertanyaan'))
				->callback_after_update(array($this, 'callback_after_update'))
				->set_relation('ID_User','users','email')
				->set_relation('ID_Narasumber','narasumber','Nama')
				->set_relation_n_n('Kategori','kategori_pertanyaan','kategori','ID_Pertanyaan','ID_Kategori','Kategori')		
				->columns('ID_Pertanyaan','Waktu','ID_User','Judul','Pertanyaan','Kategori','ID_Narasumber','Jawaban')
				->fields('Judul','keywords','description','Pertanyaan','Kategori','ID_Narasumber','Jawaban','ID_User')
				->field_type('Url_title','hidden')
				->display_as('ID_Narasumber','Narasumber')
				->display_as('ID_User','Email Member')
				->where('Jawaban =','')
				->order_by('ID_Pertanyaan','ASC');
		$config = array(
			array(
				'field' => 'Judul',
				'label' => 'Judul',
				'rules' => 'trim|required|xss_clean|max_length[100]'
				),
			array(
				'field'=>'keywords',
				'label'=>'Keywords',
				'rules'=>'trim|required|max_length[255]|xss_clean'
				),
			array(
				'field'=>'description',
				'label'=>'Description',
				'rules'=>'trim|required|max_length[220]|xss_clean'
				),
			array(
				'field' => 'Pertanyaan',
				'label' => 'Pertanyaan',
				'rules' => 'required'
				)
			);
		$crud->set_rules($config);
		$output = $crud->render();
		$this->print_output($output);
	}
	//Pertanyaan telah terjawab
	public function pertanyaan()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('pertanyaan')
				->set_subject('Pertanyaan')
				->callback_after_update(array($this, 'url_title_pertanyaan'))
				->unset_add()
				->set_relation('ID_User','users','email')
				->set_relation('ID_Narasumber','narasumber','Nama')
				->set_relation_n_n('Kategori','kategori_pertanyaan','kategori','ID_Pertanyaan','ID_Kategori','Kategori')		
				->columns('ID_Pertanyaan','Waktu','ID_User','Judul','Pertanyaan','Jawaban')
				->fields('Judul','keywords','description','Pertanyaan','Kategori','ID_Narasumber','Jawaban','ID_User')
				->display_as('ID_Narasumber','Narasumber')
				->display_as('ID_User','Email Member')
				->where('Jawaban !=','')
				->order_by('ID_Pertanyaan','DESC');
		$config = array(
			array(
				'field' => 'Judul',
				'label' => 'Judul',
				'rules' => 'trim|required|xss_clean|max_length[100]'
				),
			array(
				'field'=>'keywords',
				'label'=>'Keywords',
				'rules'=>'trim|required|max_length[255]|xss_clean'
				),
			array(
				'field'=>'description',
				'label'=>'Description',
				'rules'=>'trim|required|max_length[220]|xss_clean'
				),
			array(
				'field' => 'Pertanyaan',
				'label' => 'Pertanyaan',
				'rules' => 'required|xss_clean'
				),
			array(
				'field' => 'Jawaban',
				'label' => 'Jawaban',
				'rules' => 'xss_clean'
				)
			);
		$crud->set_rules($config);
		$output = $crud->render();
		$this->print_output($output);
	}

	public function callback_after_update($post_array,$primary_key)
	{
		if(!isset($post_array) && empty($post_array))
		{
			show_404();
		}
		$this->email_notifikasi_jawaban($post_array, $primary_key);
		$this->url_title_pertanyaan($post_array, $primary_key);
	}

	public function email_notifikasi_jawaban($post_array,$primary_key)
	{
		if(!isset($post_array) && empty($post_array))
		{
			show_404();
		}
		$message = $this->load->view('templates/email_jawaban', $post_array, true);
		$this->load->model('ion_auth_model');
		$data_user = $this->ion_auth_model->user($post_array['ID_User'])->row();

		$this->load->library('email');
		$this->email->clear();
		$this->email->from($this->config->item('admin_email', 'ion_auth'), $this->config->item('site_title', 'ion_auth'));
		$this->email->to($data_user->email);
		$this->email->subject($this->lang->line('email_answer_subject'));
		$this->email->message($message);
		$this->email->send();
	}

	public function url_title_pertanyaan($post_array, $primary_key)
	{
		if(!isset($post_array) && empty($post_array))
		{
			show_404();
		}
		$url_title = url_title($post_array['Judul'], '_', TRUE);
		$this->load->model('pertanyaan_model');
		$this->pertanyaan_model->update_url($post_array['Judul'], $url_title);
	}

	public function user()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('user')
				->set_subject('User');
		//$crud->set_relation('Hak_akses','hak_akses','Nama');
		$crud->field_type('Hak_akses','dropdown',array('Administrator'=>'Administrator','Member'=>'Member'));
		$crud->field_type('Status','dropdown',array('Active'=>'Active','Waiting'=>'Waiting'));
		$crud->unset_fields('ID_User');
		$output = $crud->render();
		$this->print_output($output);
	}

	public function berita()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('berita')
			->set_subject('Berita')
			->callback_after_insert(array($this, 'url_title_berita'))
			->callback_after_update(array($this, 'url_title_berita'))
			->columns('Judul', 'Berita', 'Foto', 'Video', 'Video_utama')
			->fields('Judul','keywords','description','Berita','Foto','Video','Video_utama')
			->display_as('Video_utama','Video Utama')
			->field_type('Video_utama','dropdown',array('Ya'=>'Ya'))
			->set_field_upload('Foto','assets/images/berita')
			->order_by('ID_Berita','DESC');
		$config = array(
			array(
				'field'=>'Judul',
				'label'=>'Judul',
				'rules'=>'trim|required|max_length[50]|xss_clean'
				),
			array(
				'field'=>'keywords',
				'label'=>'Keywords',
				'rules'=>'trim|required|max_length[255]|xss_clean'
				),
			array(
				'field'=>'description',
				'label'=>'Description',
				'rules'=>'trim|required|max_length[220]|xss_clean'
				),
			array(
				'field'=>'Berita',
				'label'=>'Berita',
				'rules'=>'required|xss_clean'
				),
			array(
				'field'=>'Foto',
				'label'=>'Foto',
				'rules'=>'prep_for_form'
				),
			array(
				'field'=>'Video_utama',
				'label'=>'Video Utama',
				'rules'=>'trim|max_length[5]|xss_clean'
				)
			);
		$crud->set_rules($config);
		$output = $crud->render();
		$this->print_output($output);
	}

	public function url_title_berita($post_array, $primary_key)
	{
		if(!isset($post_array) && empty($post_array))
		{
			show_404();
		}
		$url_title = url_title($post_array['Judul'], '_', TRUE);
		$this->load->model('berita_model');
		$this->berita_model->update_url($post_array['Judul'], $url_title);
	}

	public function opini()
	{
		$crud = new grocery_CRUD();
		$config = array(
			array(
				'field'=>'Judul',
				'label'=>'Judul',
				'rules'=>'trim|required|max_length[100]|xss_clean'
				),
			array(
				'field'=>'keywords',
				'label'=>'Keywords',
				'rules'=>'trim|required|max_length[255]|xss_clean'
				),
			array(
				'field'=>'description',
				'label'=>'Description',
				'rules'=>'trim|required|max_length[220]|xss_clean'
				),
			array(
				'field'=>'Opini',
				'label'=>'Opini',
				'rules'=>'trim|required|xss_clean'
				),
			array(
				'field'=>'Nama_narasumber',
				'label'=>'Nama Narasumber',
				'rules'=>'trim|required|max_length[100]|xss_clean'
				),
			array(
				'field'=>'Foto',
				'label'=>'Foto',
				'rules'=>'required'
				)
			);
		$crud->set_table('opini')
				->set_subject('Opini')
				->callback_after_insert(array($this, 'url_title_opini'))
				->callback_after_update(array($this, 'url_title_opini'))
				->columns('Waktu','Judul','Opini','Nama_narasumber','Foto','Status')
				->fields('Judul','keywords','description','Nama_narasumber','Opini','Foto','Status')
				->display_as('Nama_narasumber','Nama Narasumber')
				->set_field_upload('Foto','assets/images/opini')
				->field_type('Status','dropdown',array('Active'=>'Active'))
				->set_rules($config);
		$output = $crud->render();
		$this->print_output($output);
	}
	public function url_title_opini($post_array, $primary_key)
	{
		if(!isset($post_array) && empty($post_array))
		{
			show_404();
		}
		$url_title = url_title($post_array['Judul'], '_', TRUE);
		$this->load->model('berita_model');
		$this->berita_model->update_url_opini($post_array['Judul'], $url_title);
	}
	public function profil()
	{
		$crud = new grocery_CRUD();
		$config = array(
			array(
				'field'=>'Nama',
				'label'=>'Nama',
				'rules'=>'trim|required|max_length[100]|xss_clean'
				),
			array(
				'field'=>'Profil',
				'label'=>'Profil',
				'rules'=>'trim|required|xss_clean'
				),
			array(
				'field'=>'Foto',
				'label'=>'Foto',
				'rules'=>'required'
				)
			);
		$crud->set_table('profil')
				->set_subject('Profil')
				->fields('Nama','Profil','Foto','Video')
				->set_field_upload('Foto','assets/images/profil')
				->set_rules($config);
		$output = $crud->render();
		$this->print_output($output);
	}
	public function narasumber()
	{
		$crud= new grocery_CRUD();
		$config = array(
			array(
				'field'=>'Nama',
				'label'=>'Nama',
				'rules'=>'trim|required|max_length[100]|xss_clean'
				),
			array(
				'field'=>'Profesi',
				'label'=>'Profesi',
				'rules'=>'trim|required|max_length[50]|xss_clean'
				),
			array(
				'field'=>'Email',
				'label'=>'Email',
				'rules'=>'trim|required|max_length[50]|valid_email|xss_clean'
				)
			);
		$crud->set_table('narasumber')
				->set_subject('Narasumber')
				->fields('Nama','Profesi','Email','Foto')
				->set_field_upload('Foto','assets/images/narasumber')
				->unset_delete()
				->set_rules($config);
		$output = $crud->render();
		$this->print_output($output);
	}
	public function banner()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('banner')
				->set_subject('Iklan Banner');
		$crud->fields('Nama','Deskripsi','Poster','Mulai_pasang','Selesai_pasang','Posisi');
		$crud->field_type('Mulai_pasang','date');
		$crud->field_type('Selesai_pasang','date');
		$crud->field_type('Posisi','dropdown',array('Kanan Atas'=>'Kanan Atas','Kanan Tengah'=>'Kanan Tengah','Kanan Bawah'=>'Kanan Bawah','Kiri Bawah'=>'Kiri Bawah','Hal Berita'=>'Hal Berita'));
		$crud->set_field_upload('Poster','assets/images/banner');
		$config = array(
			array(
				'field'=>'Nama',
				'label'=>'Nama',
				'rules'=> 'trim|required|max_length[50]|xss_clean'
				)
			);
		$crud->set_rules($config);
		$output = $crud->render();
		$this->print_output($output);
	}

	public function print_output($output=null)
	{
		$this->load->view('admin/admin_view', $output);
	}

}
/* End of file admin.php */
/* Location: ./application/controllers/admin.php */